<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Menu{
    /* --------------------------------------------------------------------------------------------------------------------------------------- */
		public function bootstrap_menu($array,$parent_id = 0,$parents = array())
    {
      if($parent_id==0)
      {
        foreach ($array as $element) {
          if (($element['parent_id'] != 0) && !in_array($element['parent_id'],$parents)) {
            $parents[] = $element['parent_id'];
          }
        }
      }
      $menu_html = '';
      foreach($array as $element)
      {
        if($element['parent_id']==$parent_id)
        {
          if(in_array($element['id'],$parents))
          {
            $menu_html .= '<li class="dropdown">';
            $menu_html .= '<a href="'.site_url($element['url']).'" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i style="color: #000 !important;" class="'.$element['icon'].'"></i><p style="color: #000 !important;">'.$element['title'].'<span class="caret"></span></p></a>';
          }
          else {
            $menu_html .= '<li>';
            $menu_html .= '<a href="'.site_url($element['url']).'"><i class="'.$element['icon'].'"></i><p>'.$element['title'].'</p></a>';
          }
          if(in_array($element['id'],$parents))
          {
            $menu_html .= '<ul class="dropdown-menu" role="menu">';
            $menu_html .= $this->bootstrap_menu($array, $element['id'], $parents);
            $menu_html .= '</ul>';
          }
        }
      }
      return $menu_html;   
    }
    /* --------------------------------------------------------------------------------------------------------------------------------------- */
	}
?>