<?php
/*
*	Esta función devuelve el título de la aplicación
*/
	function h_getTitulo_Pagina(){
		/* ------- Se muestra en header.php como título de página ------- */
		return 'Plaza Motos';
		/* ------- ./Se muestra en header.php como título de página ------- */
	}
/* ---------------------------------------------------------------------------- */
/*
*	Esta función devuelve el título de la pantalla, se pueden repetir esta función agregando un sufijo al nombre de la
*	función, para otorgar títulos a diferentes vistas.
*/
	function h_getTitulo(){
		/* ------- Se muestra en header.php como título de página ------- */
		return 'Plaza Motos';
		/* ------- ./Se muestra en header.php como título de página ------- */
	}
/* ---------------------------------------------------------------------------- */
/* 
*	Esta función devuelve el arreglo para el menú, de acuerdo al rol del usuario.	
*/
	function h_getMenu(){
		$arr_b_m = array(
		    			 array("id"=>1,"parent_id"=>0,"title"=>"","icon"=>"fa fa-home","url"=>'inicio/c_inicio'),
						 	 array("id"=>6,"parent_id"=>0,"title"=>"Cerrar Sesión","icon"=>"","url"=>'inicio/c_inicio/cerrarSesion')
		);
		return $arr_b_m;
	}
/* ---------------------------------------------------------------------------- */	
/*
*	Esta función devuelve la imagen del encabezado.
*/
	function h_img_encabezado(){
		$img_encabezado = array(
		        									'src'   => 'assets/imagenes/new_plazamotos2.png',
											        'alt'   => 'Plaza Motos',
											        'class' => 'img-responsive',
											        'width' => '100%',
											        'height'=> '',
											        'title' => 'Plaza Motos',
											        'rel'   => '');
		return $img_encabezado;
	}
/* ---------------------------------------------------------------------------- */	
/*
*	Esta función devuelve la imagen del encabezado 2.
*/
	function h_img_encabezado_2(){
		$img_encabezado_2 = array(
		        									'src'   => 'assets/imagenes/new_2_plazamotos2.png',
											        'alt'   => 'Plaza Motos',
											        'class' => 'img-responsive',
											        'width' => '100%',
											        'height'=> '',
											        'title' => 'Plaza Motos',
											        'rel'   => '');
		return $img_encabezado_2;
	}
/* ---------------------------------------------------------------------------- */	
/*
*	Esta función devuelve la imagen del encabezado 2.
*/
	function h_img_plazamotos(){
		$img_plazamotos = array(
		        									'src'   => 'assets/imagenes/plazaMotos.png',
											        'alt'   => 'Plaza Motos',
											        'class' => '',
											        'width' => '70%',
											        'height'=> '',
											        'title' => 'Plaza Motos',
											        'rel'   => '');
		return $img_plazamotos;
	}
/* ---------------------------------------------------------------------------- */	
/*
*	Esta función devuelve la imagen del about.
*/
	function h_img_about(){
		$img_plazamotos = array(
		        									'src'   => 'assets/imagenes/about.png',
											        'alt'   => 'Plaza Motos',
											        'class' => '',
											        'width' => '100%',
											        'height'=> '',
											        'title' => 'Plaza Motos',
											        'rel'   => '');
		return $img_plazamotos;
	}
/* ---------------------------------------------------------------------------- */
/*
*	Esta función devuelve la imagen del pie de página.
*/
	function h_img_pie_de_pagina(){
		$img_pie_de_pagina = array(
												        'src'   => '',
												        'alt'   => '',
												        'class' => 'flotar-arriba-footer',
												        'width' => '100%',
												        'height'=> '4px',
												        'title' => '',
												        'rel'   => '');
		return $img_pie_de_pagina;
	}
/* ---------------------------------------------------------------------------- */
/*
*	Esta función devuelve la imagen del pie de página.
*/
	function h_img_app_logo(){
		$img_app_logo = array(
												        'src'   => 'assets/imagenes/plazaMotos.png',
												        'alt'   => 'Plaza Motos',
												        'class' => 'img-responsive',
												        'width' => '350px',
												        'height'=> '200px',
												        'title' => 'Plaza Motos',
												        'rel'   => '');
		return $img_app_logo;
	}
/* ---------------------------------------------------------------------------- */
/*
*	Plazamotos 10 años
*/
	function h_img_plazamotos_10(){
		$img_plazamotos_10 = array(
												        'src'   => 'assets/imagenes/plazamotos_10.png',
												        'alt'   => 'Plaza Motos',
												        'class' => 'img-responsive',
												        'title' => 'Plaza Motos',
												        'rel'   => '');
		return $img_plazamotos_10;
	}
/* ---------------------------------------------------------------------------- */
/*
*	Plazamotos 10 años
*/
	function h_img_2_plazamotos_10(){
		$img_plazamotos_10 = array(
												        'src'   => 'assets/imagenes/plazamotos_10.png',
												        'alt'   => 'Plaza Motos',
												        'class' => '',
												        'width' => '50%',
												        'title' => 'Plaza Motos',
												        'rel'   => '');
		return $img_plazamotos_10;
	}
/* ---------------------------------------------------------------------------- */
function h_ordena_listado_combo($data){
	$i = 0;      
    if($data!= null){
        if(count($data) > 0){ 
            $combo = "<option value='0'>SELECCIONE</option>";
        }//if

        // se agrega opciones al combo      
        while($i < count($data)){           
            $combo.= "<option value='".$data[$i]['id']."'>".$data[$i]["valor"]."</option>";
            $i++;
        }//while   
        return $combo;          
    }
    else{
        return -1;          
    }
    
}
?>