<?php
  Class Extra extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaExtra($extra,$monto){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.extra, ext.monto
                 FROM extras ext
                 WHERE 1=1";
      
      if($extra != "0"){
        $cadena.= " AND ext.extra LIKE '%$extra%'";
      }
      if($monto != "0"){
        $cadena.= " AND ext.monto = '$monto'";
      }
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['extra'].'</td>';
          $lista_datos .= '<td>'.$row['monto'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarExtra($extra,$monto){      
      $data = array(
          'extra'    => $extra,
          'monto'        => $monto,
      );

      $resultado = $this->db->insert('extras', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca extra por id
    //----------------------------------------------------------------------
    function consultarExtraRegistro($id){
         
      $sql    = "SELECT ext.id, ext.extra, ext.monto
                 FROM extras ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "extra"  =>    $row['extra'],
            "monto"      =>    $row['monto'],
            "id"         =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Extra
    //----------------------------------------------------------------------
    function editarRegistroExtra($extra,$monto,$id){
      $data = array(
          'extra'    => $extra,
          'monto'        => $monto,
      );

      $this->db->where('id', $id);
      $this->db->update('extras', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Extra
    //---------------------------------------------------------------------- 
    function limpiarRegistroExtra($id){
      $sql = "SELECT * FROM extras d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('extras');
        return 1;
      }//if   
      else {
        return 2; // Este Extra no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>