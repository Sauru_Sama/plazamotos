    <div class="container-fluid">
        <div class="card card-container">
             <?php echo validation_errors(); ?>
            <?php $attributes = array('class' => 'form-signin', 'id' => 'frm_login', 'name'=>'frm_login', 'autocomplete'=>'off'); ?>
            <?php echo form_open('login/c_verifica_login', $attributes); ?>
                <span id="reauth-name" class="reauth-name"></span>
                <input type="text" id="username" name="username" placeholder="Usuario" class="form-control" required autofocus/>
                <input type="password" id="password" name="password" placeholder="Clave" class="form-control" required/>
                <input name="tx_intento" type="hidden" id="tx_intento" value="0" size="5" maxlength="5">
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Entrar</button>
            </form><!-- /form -->
        </div><!-- /card-container -->
    </div><!-- /container-fluid -->