<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_verifica_login extends MX_Controller {
	function __construct(){
		parent::__construct();

		/* ------- h_plantilla_helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./h_plantilla_helper ------- */

		/* ------- h_plantilla_helper ------- */
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		/* ------- ./h_plantilla_helper ------- */

		$this->load->model('login/personas','',TRUE);

	}//function
//----------------------------------------------------------------------
	function index(){
		$this->load->library('form_validation');
		 
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required|callback_check_database');
		 
		if($this->form_validation->run() == FALSE){
		    /* ------- Título de Página y Subtitulo ------- */
			$data['el_titulo'] = 'Inicio de Sesi&oacute;n';
			$data['la_palabra'] = 'Sesi&oacute;n';
			/* ------- ./Título de Página y Subtitulo ------- */

			/* ------- Carga de Imágenes usando la libreria html_helper ------- */
			$data['img_app_logo'] = h_img_app_logo();
			$data['img_fondo_encabezado'] = h_img_encabezado();
			$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
			/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

			$this->load->helper(array('form'));
			
			/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
			$this->load->view('plantillas/header_login',$data);
			$this->load->view('login/login',$data['el_titulo']);
			$this->load->view('plantillas/footer');
		}
		else{
		    redirect('inicio', 'refresh');
		}
	}//function
//----------------------------------------------------------------------
	function check_database($password){
	  	$username = $this->input->post('username');
	  	$username = strtoupper($username);

		$result = $this->personas->inicioSesion($username, $password);
						 
	  	if($result){
	  		$sess_array = array();
	    	foreach($result as $row){
	    		$sess_array = array(
					'id'        =>		$row->id,
					'indicador' =>		$username
	    		);
	    		$this->session->set_userdata('Conectado', $sess_array);
	  		}//foreach
	  		return TRUE;
	  	}	
	  	else{
			    $this->form_validation->set_message('check_database','<script>error_login();</script>');
			    return false;
			}
	}//function
//----------------------------------------------------------------------
}//class

