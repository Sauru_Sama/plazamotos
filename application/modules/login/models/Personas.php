<?php
Class Personas extends CI_Model
{
  function inicioSesion($username, $password){
    $sql = "SELECT a.id, a.indicador, a.clave
            FROM personas a
            WHERE a.indicador = ?";     

    $query = $this->db->query($sql, array($username));     
    
    if ($query->num_rows() > 0){
      $hash = $query->first_row();
      if (password_verify($password, $hash->clave)) {
        return $query->result();
      } else {
        return false;
      }
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function
}//class
?>