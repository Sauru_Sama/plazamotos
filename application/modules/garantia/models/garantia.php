<?php
  Class Garantia extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaGarantia($garantia,$factor){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.garantia, ext.factor
                 FROM garantias ext
                 WHERE 1=1";
      
      if($garantia != "0"){
        $cadena.= " AND ext.garantia LIKE '%$garantia%'";
      }
      if($factor != "0"){
        $cadena.= " AND ext.factor = '$factor'";
      }
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['garantia'].'</td>';
          $lista_datos .= '<td>'.$row['factor'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarGarantia($garantia,$factor){      
      $data = array(
          'garantia'    => $garantia,
          'factor'        => $factor,
      );

      $resultado = $this->db->insert('garantias', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca garantia por id
    //----------------------------------------------------------------------
    function consultarGarantiaRegistro($id){
         
      $sql    = "SELECT ext.id, ext.garantia, ext.factor
                 FROM garantias ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "garantia"  =>    $row['garantia'],
            "factor"      =>    $row['factor'],
            "id"         =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Garantia
    //----------------------------------------------------------------------
    function editarRegistroGarantia($garantia,$factor,$id){
      $data = array(
          'garantia'    => $garantia,
          'factor'        => $factor,
      );

      $this->db->where('id', $id);
      $this->db->update('garantias', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Garantia
    //---------------------------------------------------------------------- 
    function limpiarRegistroGarantia($id){
      $sql = "SELECT * FROM garantias d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('garantias');
        return 1;
      }//if   
      else {
        return 2; // Este Garantia no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>