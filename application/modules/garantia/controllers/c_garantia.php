<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_garantia extends MX_Controller {
function __construct(){
		parent::__construct();
		/* ------- helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./helper------ */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
	  	$this->form_validation->CI =& $this;
	  	/* ------- ./form_validation ------- */

	  	/* ------- Modelo --------- */
	  	$this->load->model('menus');
	  	$this->load->model('garantia');
	  	/* ------- ./Modelo ------- */
		
	}//function
//---------------------------------------------------------------------- 
	public function index(){
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('garantia/garantia_consulta');		
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function consultarGarantia(){    
		$v_garantia = $this->input->post('garantia');
		$v_factor = $this->input->post('factor');
	    $consulta = $this->garantia->consultaGarantia($v_garantia,$v_factor);         
	    $data["tabla_consulta"] = $consulta;	     

  	echo ($this->load->view('garantia/listado_garantia',$data));
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function cargarGarantiaRegistro(){    
   		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
   
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('garantia/garantia_registro');		
		$this->load->view('plantillas/footer');    	
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function editarGarantia(){    
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
		/* -------  ------- */
		$id = $this->uri->segment(3);
    	$consultarGarantia = $this->garantia->consultarGarantiaRegistro($id); 
    	$datos["datos_garantia"] = $consultarGarantia;
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('garantia/garantia_editar_registro',$datos);		
		$this->load->view('plantillas/footer');    
	
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function guardarGarantia(){    
		$v_garantia= $this->input->post('garantia');
		$v_factor= $this->input->post('factor');
    	echo $consulta = $this->garantia->guardarGarantia($v_garantia,$v_factor);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function editarRegistroGarantia(){
		$v_garantia= $this->input->post('garantia');
		$v_factor= $this->input->post('factor');
		$v_id= $this->input->post('id');    
    	echo $consulta = $this->garantia->editarRegistroGarantia($v_garantia,$v_factor,$v_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function limpiarRegistroGarantia($v_id){    
    	echo $consulta = $this->garantia->limpiarRegistroGarantia($v_id);
	}//function	
	//----------------------------------------------------------------------
	function cerrarSesion(){
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	  	session_destroy();
	  	redirect('login/c_verifica_login', 'refresh');
	}	
//----------------------------------------------------------------------
}//class