<script>
  var baseurl = "<?php print base_url(); ?>";
  $(document).ready(function(){
    //------------------------------------------------------
    function registro_formulario(){
      v_plan        = $("#v_plan").val().trim().toUpperCase();
      v_concatenar  = $("#v_concatenar").val().trim().toUpperCase();
      v_cuota       = $("#v_cuota").val().trim();
      v_coeficiente = $("#v_coeficiente").val().trim();
      v_tarjeta     = $("#v_tarjeta").val().trim();
      
      if ((v_plan == "") && (v_concatenar == "") && (v_cuota == "") && (v_coeficiente == "") && (v_tarjeta == "")) {
        $("#v_plan").addClass("rojo-requerido"); 
        $("#v_concatenar").addClass("rojo-requerido"); 
        $("#v_cuota").addClass("rojo-requerido"); 
        $("#v_coeficiente").addClass("rojo-requerido"); 
        $("#v_tarjeta").addClass("rojo-requerido"); 
      }
      else if(v_plan == ""){
        $("#v_plan").addClass("rojo-requerido");  
      }
      else if(v_concatenar == ""){
        $("#v_concatenar").addClass("rojo-requerido");  
      }
      else if(v_cuota == ""){
        $("#v_cuota").addClass("rojo-requerido");  
      }
      else if(v_coeficiente == ""){
        $("#v_coeficiente").addClass("rojo-requerido");  
      }
      else if(v_tarjeta == ""){
        $("#v_tarjeta").addClass("rojo-requerido");  
      }
      else {
        params = {
          'plan': v_plan,
          'concatenar': v_concatenar,
          'cuota': v_cuota,
          'coeficiente': v_coeficiente,
          'tarjeta': v_tarjeta,
        };

        $.ajax({
          type: 'POST',
          url: baseurl+"plan/c_plan/guardarPlan",
          data: params,
          success: function(data){                
            if(data == 1){
              exito_guardar();
              $("input[type='text']").val("");
            }          
            else{
              error_guardar();
            }
            
          }, //success
          error: function( jqXhr, textStatus, errorThrown )
          {
            console.log( textStatus+" = "+errorThrown );        
          }//error
        });//ajax
      }//else
    }//funcion     
    //------------------------------------------------------
    $("#b_usuario_guardar").click(function(){
      registro_formulario();
    });
    //------------------------------------------------------
    $("#b_usuario_limpiar").click(function(){
      $("input[type='text']").removeClass("rojo-requerido");
      $("select").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
    $("input[type='text']").change(function(){
      $("input[type='text']").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Planes / Registro
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-file-o"></i>&nbsp;&nbsp;Nuevo Registro</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="<?php echo base_url();?>/plan/c_plan">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_plan">Plan</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_plan" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_concatenar">Concatenar</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_concatenar" placeholder="">
                </div>
              </div>          

              <div class="col-md-4">
                <label for="v_cuota">Cuota</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_cuota" placeholder="">
                </div>
              </div>             

            </fieldset>     
            <div class="spacer5"></div>
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_coeficiente">Coeficiente</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_coeficiente" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_tarjeta">Tarjeta</label>
                <div class="col-md-9 pull-right">
                  <select class="form-control input-sm" id="v_tarjeta">   
                  <?php echo $lista_tarjeta; ?>               
                  </select>
                </div>
              </div>          

            </fieldset>        
            <!-- ///////////////////////////////////////////////////////// -->
              <div class="btn-group pull-right">               
                <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
                  <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
                </button>
                <a type="button" id="b_usuario_regresar" href="<?php echo site_url('plan'); ?>" class="btn btn-default">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Regresar
                </a>
                <button id="b_usuario_guardar" type="button" class="btn btn-warning">
                  <span class="glyphicon glyphicon-save" aria-hidden="true"></span> Guardar
                </button> 
              </div>
            <!-- ///////////////////////////////////////////////////////// -->
          </form>
          <!-- ///////////////////////////////////////////////////////// -->
      </div>
      <!-- ///////////////////////////////////////////////////////// -->
      <div id="plan_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->
    </div>
    <!-- ///////////////////////////////////////////////////////// -->
  </div>      
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->