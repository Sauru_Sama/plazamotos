<script>
  $(document).ready(function(){
    var baseurl = "<?php print base_url(); ?>";
    //------------------------------------------------------
    $("#b_plan_limpiar").on('click', function() {
      $("#plan_consulta_listado").empty();
    });

    function consulta_formulario(){
      v_plan        = $("#v_plan").val().trim().toUpperCase();
      v_concatenar  = $("#v_concatenar").val().trim().toUpperCase();
      v_cuota       = $("#v_cuota").val().trim();
      v_coeficiente = $("#v_coeficiente").val().trim();
      v_tarjeta     = $("#v_tarjeta").val().trim();
      
      if(v_plan == ""){
        v_plan = 0;
      }
      if(v_concatenar == ""){
        v_concatenar = 0;
      }
      if(v_cuota == ""){
        v_cuota = 0;
      }
      if(v_coeficiente == ""){
        v_coeficiente = 0;
      }
      if(v_tarjeta == ""){
        v_tarjeta = 0;
      }

      var params = {
        'plan':v_plan,
        'concatenar':v_concatenar,
        'cuota':v_cuota,
        'coeficiente':v_coeficiente,
        'tarjeta':v_tarjeta,
      };

      $.ajax({
        type: 'POST',
        url: baseurl+"plan/c_plan/consultarPlan",
        data: params,
        success: function(data){              
          $("#plan_consulta_listado").html(data);
          $('#tabla_plan_listado').dataTable( { 
            "sPaginationType": "full_numbers",
            "scrollY":        "255px",
            "scrollCollapse": false,   
            "bAutoWidth": false,
            "bDestroy": true,  
            "responsive": true,                              
            "language": {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
                "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "&Uacute;ltimo",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              },
            },   
            "order": [[ 4, "asc" ]],
            "columnDefs": [
              {
                "targets": [ 5 ],
                "visible": false,
                "searchable": false
              },
              {
                "targets": [ 6 ],
                "searchable": false,
                "orderable": false
              }
            ],
          });    
          var table = $("#tabla_plan_listado").DataTable(); // importante!
          //  Escucha al botón, toma el valor de la columna con el id en la fila donde esté y la pasa al enlace y redirige.
          $("#tabla_plan_listado tbody").on("click", ".btn-editar", function (e) {
          var data   = table.row( $(this).parents("tr") ).data();                                                  
          var vj_plan_id    = data[5].trim();
          newPage = baseurl+"plan/editar/"+vj_plan_id;
          window.location.href = newPage;              
          } );
          //  Escucha al botón, toma el valor de la columna con el id en la fila donde esté y la pasa al enlace y redirige.
          $("#tabla_plan_listado tbody").on("click", ".btn-eliminar", function (e) {
          var data   = table.row( $(this).parents("tr") ).data();                                                  
          var vj_plan_id    = data[5].trim();
          var esto = $(this).parents("tr");      
          
          sweetAlert({
              title: "¿Desea eliminar el Plan?",
              text: "Esta acción no se puede deshacer",
              type: "warning",
              showCancelButton: true,
              cancelButtonText: "No, no gracias.",
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Si, estoy seguro.",
              closeOnConfirm: false
            },
            function(){
              $.ajax({
                type: 'POST',
                url: baseurl+"plan/c_plan/limpiarRegistroPlan/"+vj_plan_id,
                type: 'json',
                success: function(data){                
                  if(data == 1){
                    sweetAlert("Exito.",'Se eliminaron satisfacoriamente los datos.', 'success');
                  }          
                  else{
                    sweetAlert('Disculpe.', 'Hubo problemas al intentar eliminar los datos', 'error');
                  }                              
                }, //success
                error: function( jqXhr, textStatus, errorThrown ){
                console.log( textStatus+" = "+errorThrown);        
                }//error
              });//ajax 
              table
                .row( esto )
                .remove()
                .draw();        
            });    
          } );

          $( $.fn.dataTable.tables( true ) ).DataTable().columns.adjust();              
        }, //success
        error: function( jqXhr, textStatus, errorThrown ){
        console.log( textStatus+" = "+errorThrown);        
        }//error
      });//ajax
    }//funcion     
    //------------------------------------------------------
    $("#b_usuario_buscar").click(function(){
      consulta_formulario();
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  <div id="confirmar_deshabilitar" title="Confirmaci&oacute;n"></div>
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Planes / Consultar
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-search"></i>&nbsp;&nbsp;Consulta</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="c_plan/cargarPlanRegistro">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_plan">Plan</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_plan" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_concatenar">Concatenar</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_concatenar" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_cuota">Cuota</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_cuota" placeholder="">
                </div>
              </div>

            </fieldset>
            <div class="spacer5"></div>
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_coeficiente">Coeficiente</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_coeficiente" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_tarjeta">Tarjeta</label>
                <div class="col-md-9 pull-right">
                  <select class="form-control input-sm" id="v_tarjeta">   
                  <?php echo $lista_tarjeta; ?>               
                  </select>
                </div>
              </div>

            </fieldset>

            <!-- ///////////////////////////////////////////////////////// -->
            <div class="btn-group pull-right">
              <button id="b_usuario_buscar" type="button" class="btn btn-default">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar
              </button>
              <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
                <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
              </button>
              <a type="button" id="b_usuario_agregar" href="<?php echo site_url('plan/nuevo'); ?>" class="btn btn-warning">
                <span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> Agregar
              </a>      
            </div>
            <!-- ///////////////////////////////////////////////////////// -->
            
          </form>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div id="plan_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->

    </div>

  </div>
  
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div id="reporte_detalle_vista_previa"></div>       
<!-- ///////////////////////////////////////////////////////// -->