<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_plan extends MX_Controller {
	function __construct(){
		parent::__construct();
		/* ------- helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./helper------ */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
	  	$this->form_validation->CI =& $this;
	  	/* ------- ./form_validation ------- */

	  	/* ------- Modelo --------- */
	  	$this->load->model('menus');
	  	$this->load->model('plan');
	  	/* ------- ./Modelo ------- */
		
	}//function
//---------------------------------------------------------------------- 
	public function index(){
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();

		$consultaTarjeta = $this->plan->consultarTarjeta(); 
	  	$datos["lista_tarjeta"] = h_ordena_listado_combo($consultaTarjeta);
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('plan/plan_consulta',$datos);		
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function consultarPlan(){    
		$v_plan = $this->input->post('plan');
		$v_concatenar = $this->input->post('concatenar');
		$v_cuota = $this->input->post('cuota');
		$v_coeficiente = $this->input->post('coeficiente');
		$v_tarjeta_id = $this->input->post('tarjeta');
    	$consulta = $this->plan->consultaPlan($v_plan,$v_concatenar,$v_cuota,$v_coeficiente,$v_tarjeta_id);         
    	$data["tabla_consulta"] = $consulta;	     

  		echo ($this->load->view('plan/listado_plan',$data));
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function cargarPlanRegistro(){    
   		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();

		$consultaTarjeta = $this->plan->consultarTarjeta(); 
	  	$datos["lista_tarjeta"] = h_ordena_listado_combo($consultaTarjeta);
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
   
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('plan/plan_registro',$datos);		
		$this->load->view('plantillas/footer');    	
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function editarPlan(){    
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
		/* -------  ------- */
		$id = $this->uri->segment(3);
    	$consultarPlan = $this->plan->consultarPlanRegistro($id); 
    	$datos["datos_plan"] = $consultarPlan;

    	$consultaTarjeta = $this->plan->consultarTarjeta(); 
	  	$datos["lista_tarjeta"] = h_ordena_listado_combo($consultaTarjeta);
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('plan/plan_editar_registro',$datos);		
		$this->load->view('plantillas/footer');    
	
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function guardarPlan(){    
		$v_plan = $this->input->post('plan');
		$v_concatenar = $this->input->post('concatenar');
		$v_cuota = $this->input->post('cuota');
		$v_coeficiente = $this->input->post('coeficiente');
		$v_tarjeta_id = $this->input->post('tarjeta');
   		echo $consulta = $this->plan->guardarPlan($v_plan,$v_concatenar,$v_cuota,$v_coeficiente,$v_tarjeta_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function editarRegistroPlan(){
		$v_plan = $this->input->post('plan');
		$v_concatenar = $this->input->post('concatenar');
		$v_cuota = $this->input->post('cuota');
		$v_coeficiente = $this->input->post('coeficiente');
		$v_tarjeta_id = $this->input->post('tarjeta');
		$v_id= $this->input->post('id');    
    	echo $consulta = $this->plan->editarRegistroPlan($v_plan,$v_concatenar,$v_cuota,$v_coeficiente,$v_tarjeta_id,$v_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function limpiarRegistroPlan($v_id){    
    	echo $consulta = $this->plan->limpiarRegistroPlan($v_id);
	}//function	
	//----------------------------------------------------------------------
	function cerrarSesion(){
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	  	session_destroy();
	  	redirect('login/c_verifica_login', 'refresh');
	}	
//----------------------------------------------------------------------
}//class