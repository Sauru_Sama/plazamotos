<?php
  Class Plan extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaPlan($plan,$concatenar,$cuota,$coeficiente,$tarjeta_id){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.tarjeta_id, ext.plan, ext.concatenar, ext.cuota, ext.coeficiente, ope.tarjeta
                 FROM tarjetas_planes ext, tarjetas ope
                 WHERE ext.tarjeta_id = ope.id";
      
      if($plan != "0"){
        $cadena.= " AND ext.plan LIKE '%$plan%'";
      }
      if($concatenar != "0"){
        $cadena.= " AND ext.concatenar LIKE '%$concatenar%'";
      }
      if($cuota != "0"){
        $cadena.= " AND ext.cuota = '$cuota'";
      }
      if($coeficiente != "0"){
        $cadena.= " AND ext.coeficiente = '$coeficiente'";
      }
      if($tarjeta_id != "0"){
        $cadena.= " AND ext.tarjeta_id = $tarjeta_id";
      }
      $cadena.= " ORDER BY ope.tarjeta ASC";
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['plan'].'</td>';
          $lista_datos .= '<td>'.$row['concatenar'].'</td>';
          $lista_datos .= '<td>'.$row['cuota'].'</td>';
          $lista_datos .= '<td>'.$row['coeficiente'].'</td>';
          $lista_datos .= '<td>'.$row['tarjeta'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //---------------------------------------------------------------------- 
    //  Obtiene listado de tarjetas
    //----------------------------------------------------------------------  
    function consultarTarjeta(){

      $sql = "SELECT ro.tarjeta, ro.id
              FROM tarjetas ro
              ORDER BY ro.tarjeta ASC";     

      $query = $this->db->query($sql);     

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 0;
        foreach ($query->result_array() as $reg){
          $v_resultado [$i]['id']    = $reg['id'];
          $v_resultado [$i]['valor'] = $reg['tarjeta'];
          $i++;
        }//foreach
      }//else        

      return($v_resultado);  
      $query->free_result();
      $this->db->close(); 

    }//function  
    //----------------------------------------------------------------------
    function guardarPlan($plan,$concatenar,$cuota,$coeficiente,$tarjeta_id){      
      $data = array(
          'plan'        => $plan,
          'concatenar'  => $concatenar,
          'cuota'       => $cuota,
          'coeficiente' => $coeficiente,
          'tarjeta_id'  => $tarjeta_id,
      );

      $resultado = $this->db->insert('tarjetas_planes', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca plan por id
    //----------------------------------------------------------------------
    function consultarPlanRegistro($id){
         
      $sql    = "SELECT ext.id, ext.plan, ext.concatenar, ext.cuota, ext.coeficiente, ext.tarjeta_id
                 FROM tarjetas_planes ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "plan"        =>    $row['plan'],
            "concatenar"  =>    $row['concatenar'],
            "cuota"       =>    $row['cuota'],
            "coeficiente" =>    $row['coeficiente'],
            "tarjeta"     =>    $row['tarjeta_id'],
            "id"          =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Plan
    //----------------------------------------------------------------------
    function editarRegistroPlan($plan,$concatenar,$cuota,$coeficiente,$tarjeta_id,$id){
      $data = array(
          'plan'        => $plan,
          'concatenar'  => $concatenar,
          'cuota'       => $cuota,
          'coeficiente' => $coeficiente,
          'tarjeta_id'  => $tarjeta_id,
      );

      $this->db->where('id', $id);
      $this->db->update('tarjetas_planes', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Plan
    //---------------------------------------------------------------------- 
    function limpiarRegistroPlan($id){
      $sql = "SELECT * FROM tarjetas_planes d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('tarjetas_planes');
        return 1;
      }//if   
      else {
        return 2; // Este Plan no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>