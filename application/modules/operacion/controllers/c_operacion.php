<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_operacion extends MX_Controller {
function __construct(){
		parent::__construct();
		/* ------- helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./helper------ */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
	  	$this->form_validation->CI =& $this;
	  	/* ------- ./form_validation ------- */

	  	/* ------- Modelo --------- */
	  	$this->load->model('menus');
	  	$this->load->model('operacion');
	  	/* ------- ./Modelo ------- */
		
	}//function
	//---------------------------------------------------------------------- 
	public function index(){
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('operacion/operacion_consulta');		
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function consultarOperacion(){    
		$v_operacion = $this->input->post('operacion');
    	$consulta = $this->operacion->consultaOperacion($v_operacion);         
    	$data["tabla_consulta"] = $consulta;	     

  		echo ($this->load->view('operacion/listado_operacion',$data));
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function cargarOperacionRegistro(){    
   		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
   
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('operacion/operacion_registro');		
		$this->load->view('plantillas/footer');    	
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function editarOperacion(){    
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
		/* -------  ------- */
		$id = $this->uri->segment(3);
    	$consultarOperacion = $this->operacion->consultarOperacionRegistro($id); 
    	$datos["datos_operacion"] = $consultarOperacion;
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('operacion/operacion_editar_registro',$datos);		
		$this->load->view('plantillas/footer');    
	
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function guardarOperacion(){    
		$v_operacion= $this->input->post('operacion');
    	echo $consulta = $this->operacion->guardarOperacion($v_operacion);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function editarRegistroOperacion(){
		$v_operacion= $this->input->post('operacion');
		$v_id= $this->input->post('id');    
    	echo $consulta = $this->operacion->editarRegistroOperacion($v_operacion,$v_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function limpiarRegistroOperacion($v_id){    
    	echo $consulta = $this->operacion->limpiarRegistroOperacion($v_id);
	}//function	
	//----------------------------------------------------------------------
	function cerrarSesion(){
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	 	session_destroy();
	  	redirect('login/c_verifica_login', 'refresh');
	}	
//----------------------------------------------------------------------
}//class