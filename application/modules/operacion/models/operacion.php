<?php
  Class Operacion extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaOperacion($operacion){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.operacion
                 FROM operaciones ext
                 WHERE 1=1";
      
      if($operacion != "0"){
        $cadena.= " AND ext.operacion LIKE '%$operacion%'";
      }
            
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['operacion'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarOperacion($operacion){      
      $data = array(
          'operacion'    => $operacion,
      );

      $resultado = $this->db->insert('operaciones', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca operacion por id
    //----------------------------------------------------------------------
    function consultarOperacionRegistro($id){
         
      $sql    = "SELECT ext.id, ext.operacion
                 FROM operaciones ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "operacion" =>    $row['operacion'],
            "id"    =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Operacion
    //----------------------------------------------------------------------
    function editarRegistroOperacion($operacion,$id){
      $data = array(
          'operacion'    => $operacion,
      );

      $this->db->where('id', $id);
      $this->db->update('operaciones', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Operacion
    //---------------------------------------------------------------------- 
    function limpiarRegistroOperacion($id){
      $sql = "SELECT * FROM operaciones d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('operaciones');
        return 1;
      }//if   
      else {
        return 2; // Este Operacion no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>