<?php
  Class Marca extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaMarca($marca){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.marca
                 FROM marcas ext
                 WHERE 1=1";
      
      if($marca != "0"){
        $cadena.= " AND ext.marca LIKE '%$marca%'";
      }
            
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['marca'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarMarca($marca){      
      $data = array(
          'marca'    => $marca,
      );

      $resultado = $this->db->insert('marcas', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca marca por id
    //----------------------------------------------------------------------
    function consultarMarcaRegistro($id){
         
      $sql    = "SELECT ext.id, ext.marca
                 FROM marcas ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "marca" =>    $row['marca'],
            "id"    =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Marca
    //----------------------------------------------------------------------
    function editarRegistroMarca($marca,$id){
      $data = array(
          'marca'    => $marca,
      );

      $this->db->where('id', $id);
      $this->db->update('marcas', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Marca
    //---------------------------------------------------------------------- 
    function limpiarRegistroMarca($id){
      $sql = "SELECT * FROM marcas d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('marcas');
        return 1;
      }//if   
      else {
        return 2; // Este Marca no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>