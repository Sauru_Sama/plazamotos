<script>
  $(document).ready(function(){
    var baseurl = "<?php print base_url(); ?>";
    //------------------------------------------------------
    $("#b_modelo_limpiar").on('click', function() {
      $("#modelo_consulta_listado").empty();
    });

    function consulta_formulario(){
      v_modelo      = $("#v_modelo").val().trim().toUpperCase();
      v_contado     = $("#v_contado").val().trim();
      v_financiada  = $("#v_financiada").val().trim();
      v_tarjeta     = $("#v_tarjeta").val().trim();
      v_cdo         = $("#v_cdo").val().trim();
      v_flete       = $("#v_flete").val().trim();
      v_accesorio   = $("#v_accesorio").val().trim();
      v_descripcion = $("#v_descripcion").val().trim().toUpperCase();
      v_categoria   = $("#v_categoria").val().trim().toUpperCase();
      v_marca       = $("#v_marca").val().trim();
      
      if(v_modelo == ""){
        v_modelo = 0;
      }
      if(v_contado == ""){
        v_contado = 0;
      }
      if(v_financiada == ""){
        v_financiada = 0;
      }
      if(v_tarjeta == ""){
        v_tarjeta = 0;
      }
      if(v_cdo == ""){
        v_cdo = 0;
      }
      if(v_flete == ""){
        v_flete = 0;
      }
      if(v_accesorio == ""){
        v_accesorio = 0;
      }
      if(v_descripcion == ""){
        v_descripcion = 0;
      }
      if(v_categoria == ""){
        v_categoria = 0;
      }
      if(v_marca == ""){
        v_marca = 0;
      }

      var params = {
        'modelo':v_modelo,
        'contado':v_contado,
        'financiada':v_financiada,
        'tarjeta':v_tarjeta,
        'cdo':v_cdo,
        'flete':v_flete,
        'accesorio':v_accesorio,
        'descripcion':v_descripcion,
        'categoria':v_categoria,
        'marca':v_marca,
      };

      $.ajax({
        type: 'POST',
        url: baseurl+"modelo/c_modelo/consultarModelo",
        data: params,
        success: function(data){              
          $("#modelo_consulta_listado").html(data);
          $('#tabla_modelo_listado').dataTable( { 
            "sPaginationType": "full_numbers",
            "scrollY":        "255px",
            "scrollCollapse": false,   
            "bAutoWidth": false,
            "bDestroy": true,  
            "responsive": true,                              
            "language": {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
                "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "&Uacute;ltimo",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              },
            },   
            "order": [[ 9, "asc" ]],
            "columnDefs": [
              {
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
              },
              {
                "targets": [ 11 ],
                "searchable": false,
                "orderable": false
              }
            ],
          });    
          var table = $("#tabla_modelo_listado").DataTable(); // importante!
          //  Escucha al botón, toma el valor de la columna con el id en la fila donde esté y la pasa al enlace y redirige.
          $("#tabla_modelo_listado tbody").on("click", ".btn-editar", function (e) {
          var data   = table.row( $(this).parents("tr") ).data();                                                  
          var vj_modelo_id    = data[10].trim();
          newPage = baseurl+"modelo/editar/"+vj_modelo_id;
          window.location.href = newPage;              
          } );
          //  Escucha al botón, toma el valor de la columna con el id en la fila donde esté y la pasa al enlace y redirige.
          $("#tabla_modelo_listado tbody").on("click", ".btn-eliminar", function (e) {
          var data   = table.row( $(this).parents("tr") ).data();                                                  
          var vj_modelo_id    = data[10].trim();
          var esto = $(this).parents("tr");      
          
          sweetAlert({
              title: "¿Desea eliminar el Modelo?",
              text: "Esta acción no se puede deshacer",
              type: "warning",
              showCancelButton: true,
              cancelButtonText: "No, no gracias.",
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Si, estoy seguro.",
              closeOnConfirm: false
            },
            function(){
              $.ajax({
                type: 'POST',
                url: baseurl+"modelo/c_modelo/limpiarRegistroModelo/"+vj_modelo_id,
                type: 'json',
                success: function(data){                
                  if(data == 1){
                    sweetAlert("Exito.",'Se eliminaron satisfacoriamente los datos.', 'success');
                  }          
                  else{
                    sweetAlert('Disculpe.', 'Hubo problemas al intentar eliminar los datos', 'error');
                  }                              
                }, //success
                error: function( jqXhr, textStatus, errorThrown ){
                console.log( textStatus+" = "+errorThrown);        
                }//error
              });//ajax 
              table
                .row( esto )
                .remove()
                .draw();        
            });    
          } );

          $( $.fn.dataTable.tables( true ) ).DataTable().columns.adjust();              
        }, //success
        error: function( jqXhr, textStatus, errorThrown ){
        console.log( textStatus+" = "+errorThrown);        
        }//error
      });//ajax
    }//funcion     
    //------------------------------------------------------
    $("#b_usuario_buscar").click(function(){
      consulta_formulario();
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  <div id="confirmar_deshabilitar" title="Confirmaci&oacute;n"></div>
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Modelos / Consultar
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-search"></i>&nbsp;&nbsp;Consulta</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="c_modelo/cargarModeloRegistro">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_modelo">Modelo</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_modelo" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_contado">Contado</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_contado" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_financiada">Financiada</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_financiada" placeholder="">
                </div>
              </div>

            </fieldset>
            <div class="spacer5"></div>
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_tarjeta">Tarjeta</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_tarjeta" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_cdo">CDO P&uacute;blico</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_cdo" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_flete">Flete</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_flete" placeholder="">
                </div>
              </div>

            </fieldset>
            <div class="spacer5"></div>
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_accesorio">Accesorio</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_accesorio" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_descripcion">Descripci&oacute;n</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_descripcion" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_categoria">Categor&iacute;a</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_categoria" placeholder="">
                </div>
              </div>

            </fieldset>
            <div class="spacer5"></div>
            <fieldset class="form-group">

              <div class="col-md-4">
                <label for="v_marca">Marca</label>
                <div class="col-md-9 pull-right">
                  <select class="form-control input-sm" id="v_marca">   
                  <?php echo $lista_marca; ?>               
                  </select>
                </div>
              </div>

            </fieldset>
            <!-- ///////////////////////////////////////////////////////// -->
            <div class="btn-group pull-right">
              <button id="b_usuario_buscar" type="button" class="btn btn-default">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar
              </button>
              <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
                <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
              </button>
              <a type="button" id="b_usuario_agregar" href="<?php echo site_url('modelo/nuevo'); ?>" class="btn btn-warning">
                <span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> Agregar
              </a>      
            </div>
            <!-- ///////////////////////////////////////////////////////// -->
            
          </form>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div id="modelo_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->

    </div>

  </div>
  
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div id="reporte_detalle_vista_previa"></div>       
<!-- ///////////////////////////////////////////////////////// -->