<script>
  var baseurl = "<?php print base_url(); ?>";
  $(document).ready(function(){
    //------------------------------------------------------
    function registro_formulario(){
      v_modelo      = $("#v_modelo").val().trim().toUpperCase();
      v_contado     = $("#v_contado").val().trim();
      v_financiada  = $("#v_financiada").val().trim();
      v_tarjeta     = $("#v_tarjeta").val().trim();
      v_cdo         = $("#v_cdo").val().trim();
      v_flete       = $("#v_flete").val().trim();
      v_accesorio   = $("#v_accesorio").val().trim();
      v_descripcion = $("#v_descripcion").val().trim().toUpperCase();
      v_categoria   = $("#v_categoria").val().trim().toUpperCase();
      v_marca       = $("#v_marca").val().trim();
      v_id          = $("#v_id").val();      
      
      if ((v_modelo == "") && (v_contado == "") && (v_financiada == "") && (v_tarjeta == "") && (v_cdo == "") && (v_flete == "") && (v_accesorio == "") && (v_descripcion == "") && (v_categoria == "") && (v_marca == "")) {
        $("#v_modelo").addClass("rojo-requerido"); 
        $("#v_contado").addClass("rojo-requerido"); 
        $("#v_financiada").addClass("rojo-requerido"); 
        $("#v_tarjeta").addClass("rojo-requerido"); 
        $("#v_cdo").addClass("rojo-requerido"); 
        $("#v_flete").addClass("rojo-requerido"); 
        $("#v_accesorio").addClass("rojo-requerido"); 
        $("#v_descripcion").addClass("rojo-requerido"); 
        $("#v_categoria").addClass("rojo-requerido"); 
        $("#v_marca").addClass("rojo-requerido"); 
      }
      else if(v_modelo == ""){
        $("#v_modelo").addClass("rojo-requerido");  
      }
      else if(v_contado == ""){
        $("#v_contado").addClass("rojo-requerido"); 
      }
      else if(v_financiada == ""){
        $("#v_financiada").addClass("rojo-requerido"); 
      }
      else if(v_tarjeta == ""){
        $("#v_tarjeta").addClass("rojo-requerido"); 
      }
      else if(v_cdo == ""){
        $("#v_cdo").addClass("rojo-requerido"); 
      }
      else if(v_flete == ""){
        $("#v_flete").addClass("rojo-requerido"); 
      }
      else if(v_accesorio == ""){
        $("#v_accesorio").addClass("rojo-requerido"); 
      }
      else if(v_descripcion == ""){
        $("#v_descripcion").addClass("rojo-requerido"); 
      }
      else if(v_categoria == ""){
        $("#v_categoria").addClass("rojo-requerido");  
      }
      else if(v_marca == ""){
        $("#v_marca").addClass("rojo-requerido");  
      }
      else {
        params = {
          'modelo': v_modelo,
          'contado': v_contado,
          'financiada': v_financiada,
          'tarjeta': v_tarjeta,
          'cdo': v_cdo,
          'flete': v_flete,
          'accesorio': v_accesorio,
          'descripcion': v_descripcion,
          'categoria': v_categoria,
          'marca': v_marca,
          'id' : v_id,
        };

        $.ajax({
          type: 'POST',
          url: baseurl+"modelo/c_modelo/guardarModelo",
          data: params,
          success: function(data){                
            if(data == 1){
              exito_guardar();
              $("input[type='text']").val("");
            }          
            else{
              error_guardar();
            }
            
          }, //success
          error: function( jqXhr, textStatus, errorThrown )
          {
            console.log( textStatus+" = "+errorThrown );        
          }//error
        });//ajax
      }//else
    }//funcion     
    //------------------------------------------------------
    $("#b_usuario_guardar").click(function(){
      registro_formulario();
    });
    //------------------------------------------------------
    $("#b_usuario_limpiar").click(function(){
      $("input[type='text']").removeClass("rojo-requerido");
      $("select").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
    $("input[type='text']").change(function(){
      $("input[type='text']").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Modelos / Registro
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-file-o"></i>&nbsp;&nbsp;Nuevo Registro</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="<?php echo base_url();?>/modelo/c_modelo">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_modelo">Modelo</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_modelo" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_contado">Contado</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_contado" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_financiada">Financiada</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_financiada" placeholder="">
                </div>
              </div>

            </fieldset>
            <div class="spacer5"></div>
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_tarjeta">Tarjeta</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_tarjeta" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_cdo">CDO P&uacute;blico</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_cdo" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_flete">Flete</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_flete" placeholder="">
                </div>
              </div>

            </fieldset>
            <div class="spacer5"></div>
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_accesorio">Accesorio</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_accesorio" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_descripcion">Descripci&oacute;n</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_descripcion" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_categoria">Categor&iacute;a</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_categoria" placeholder="">
                </div>
              </div>

            </fieldset>
            <div class="spacer5"></div>
            <fieldset class="form-group">

              <div class="col-md-4">
                <label for="v_marca">Marca</label>
                <div class="col-md-9 pull-right">
                  <select class="form-control input-sm" id="v_marca">   
                  <?php echo $lista_marca; ?>               
                  </select>
                </div>
              </div>

            </fieldset>      
            <!-- ///////////////////////////////////////////////////////// -->
              <div class="btn-group pull-right">               
                <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
                  <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
                </button>
                <a type="button" id="b_usuario_regresar" href="<?php echo site_url('modelo'); ?>" class="btn btn-default">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Regresar
                </a>
                <button id="b_usuario_guardar" type="button" class="btn btn-warning">
                  <span class="glyphicon glyphicon-save" aria-hidden="true"></span> Guardar
                </button> 
              </div>
            <!-- ///////////////////////////////////////////////////////// -->
          </form>
          <!-- ///////////////////////////////////////////////////////// -->
      </div>
      <!-- ///////////////////////////////////////////////////////// -->
      <div id="modelo_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->
    </div>
    <!-- ///////////////////////////////////////////////////////// -->
  </div>      
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->