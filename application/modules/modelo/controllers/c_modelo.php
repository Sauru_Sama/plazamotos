<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_modelo extends MX_Controller {
	function __construct(){
		parent::__construct();
		/* ------- helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./helper------ */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
	  	$this->form_validation->CI =& $this;
	  	/* ------- ./form_validation ------- */

	  	/* ------- Modelo --------- */
	  	$this->load->model('menus');
	  	$this->load->model('modelo');
	  	/* ------- ./Modelo ------- */
		
	}//function
//---------------------------------------------------------------------- 
	public function index(){
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();

		$consultaMarca = $this->modelo->consultarMarca(); 
	  	$datos["lista_marca"] = h_ordena_listado_combo($consultaMarca);
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('modelo/modelo_consulta',$datos);		
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function consultarModelo(){    
		$v_modelo      = $this->input->post('modelo');
		$v_contado     = $this->input->post('contado');
		$v_financiada  = $this->input->post('financiada');
		$v_tarjeta     = $this->input->post('tarjeta');
		$v_cdo         = $this->input->post('cdo');
		$v_flete       = $this->input->post('flete');
		$v_accesorio   = $this->input->post('accesorio');
		$v_descripcion = $this->input->post('descripcion');
		$v_categoria   = $this->input->post('categoria');
		$v_marca_id    = $this->input->post('marca');
    	$consulta = $this->modelo->consultaModelo($v_modelo,$v_contado,$v_financiada,$v_tarjeta,$v_cdo,$v_flete,$v_accesorio,$v_descripcion,$v_categoria,$v_marca_id);         
    	$data["tabla_consulta"] = $consulta;	     

  		echo ($this->load->view('modelo/listado_modelo',$data));
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function cargarModeloRegistro(){    
   		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();

		$consultaMarca = $this->modelo->consultarMarca(); 
	  	$datos["lista_marca"] = h_ordena_listado_combo($consultaMarca);
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
   
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('modelo/modelo_registro',$datos);		
		$this->load->view('plantillas/footer');    	
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function editarModelo(){    
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
		/* -------  ------- */
		$id = $this->uri->segment(3);
    	$consultarModelo = $this->modelo->consultarModeloRegistro($id); 
    	$datos["datos_modelo"] = $consultarModelo;

    	$consultaMarca = $this->modelo->consultarMarca(); 
	  	$datos["lista_marca"] = h_ordena_listado_combo($consultaMarca);
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('modelo/modelo_editar_registro',$datos);		
		$this->load->view('plantillas/footer');    
	
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function guardarModelo(){    
		$v_modelo      = $this->input->post('modelo');
		$v_contado     = $this->input->post('contado');
		$v_financiada  = $this->input->post('financiada');
		$v_tarjeta     = $this->input->post('tarjeta');
		$v_cdo         = $this->input->post('cdo');
		$v_flete       = $this->input->post('flete');
		$v_accesorio   = $this->input->post('accesorio');
		$v_descripcion = $this->input->post('descripcion');
		$v_categoria   = $this->input->post('categoria');
		$v_marca_id    = $this->input->post('marca');
    	echo $consulta = $this->modelo->guardarModelo($v_modelo,$v_contado,$v_financiada,$v_tarjeta,$v_cdo,$v_flete,$v_accesorio,$v_descripcion,$v_categoria,$v_marca_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function editarRegistroModelo(){
		$v_modelo      = $this->input->post('modelo');
		$v_contado     = $this->input->post('contado');
		$v_financiada  = $this->input->post('financiada');
		$v_tarjeta     = $this->input->post('tarjeta');
		$v_cdo         = $this->input->post('cdo');
		$v_flete       = $this->input->post('flete');
		$v_accesorio   = $this->input->post('accesorio');
		$v_descripcion = $this->input->post('descripcion');
		$v_categoria   = $this->input->post('categoria');
		$v_marca_id    = $this->input->post('marca');
		$v_id          = $this->input->post('id');    
   		echo $consulta = $this->modelo->editarRegistroModelo($v_modelo,$v_contado,$v_financiada,$v_tarjeta,$v_cdo,$v_flete,$v_accesorio,$v_descripcion,$v_categoria,$v_marca_id,$v_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function limpiarRegistroModelo($v_id){    
    	echo $consulta = $this->modelo->limpiarRegistroModelo($v_id);
	}//function	
	//----------------------------------------------------------------------
	function cerrarSesion(){
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	  	session_destroy();
	  	redirect('login/c_verifica_login', 'refresh');
	}	
//----------------------------------------------------------------------
}//class