<?php
  Class Modelo extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaModelo($modelo,$contado,$financiada,$tarjeta,$cdo,$flete,$accesorio,$descripcion,$categoria,$marca_id){ 
      $cadena = "";
      $sql    = "SELECT ext.id, ext.marca_id, ext.modelo, ext.flete, ext.accesorio, ext.descripcion, ext.categoria, ext.contado, ext.financiada, ext.tarjeta, ext.CDO_PUBLICO as cdo, mar.marca
                 FROM modelos ext, marcas mar
                 WHERE ext.marca_id = mar.id";
      
      if($modelo != "0"){
        $cadena.= " AND ext.modelo LIKE '%$modelo%'";
      }
      if($accesorio != "0"){
        $cadena.= " AND ext.accesorio = '$accesorio'";
      }
      if($descripcion != "0"){
        $cadena.= " AND ext.descripcion LIKE '%$descripcion%'";
      }
      if($categoria != "0"){
        $cadena.= " AND ext.categoria = '$categoria'";
      }
      if($contado != "0"){
        $cadena.= " AND ext.contado = '$contado'";
      }
      if($financiada != "0"){
        $cadena.= " AND ext.financiada = '$financiada'";
      }
      if($tarjeta != "0"){
        $cadena.= " AND ext.tarjeta = '$tarjeta'";
      }
      if($cdo != "0"){
        $cadena.= " AND ext.CDO_PUBLICO = '$cdo'";
      }
      if($flete != "0"){
        $cadena.= " AND ext.flete = '$flete'";
      }
      if($marca_id != "0"){
        $cadena.= " AND ext.marca_id = $marca_id";
      }
      $cadena.= " ORDER BY mar.marca ASC";
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['modelo'].'</td>';
          $lista_datos .= '<td>'.$row['contado'].'</td>';
          $lista_datos .= '<td>'.$row['financiada'].'</td>';
          $lista_datos .= '<td>'.$row['tarjeta'].'</td>';
          $lista_datos .= '<td>'.$row['cdo'].'</td>';
          $lista_datos .= '<td>'.$row['flete'].'</td>';
          $lista_datos .= '<td>'.$row['accesorio'].'</td>';
          $lista_datos .= '<td>'.$row['descripcion'].'</td>';
          $lista_datos .= '<td>'.$row['categoria'].'</td>';
          $lista_datos .= '<td>'.$row['marca'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //---------------------------------------------------------------------- 
    //  Obtiene listado de tarjetas
    //----------------------------------------------------------------------  
    function consultarMarca(){

      $sql = "SELECT ro.marca, ro.id
              FROM marcas ro
              ORDER BY ro.marca ASC";     

      $query = $this->db->query($sql);     

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 0;
        foreach ($query->result_array() as $reg){
          $v_resultado [$i]['id']    = $reg['id'];
          $v_resultado [$i]['valor'] = $reg['marca'];
          $i++;
        }//foreach
      }//else        

      return($v_resultado);  
      $query->free_result();
      $this->db->close(); 

    }//function  
    //----------------------------------------------------------------------
    function guardarModelo($modelo,$contado,$financiada,$tarjeta,$cdo,$flete,$accesorio,$descripcion,$categoria,$marca_id){      
      $data = array(
          'modelo'      => $modelo,
          'contado'     => $contado,
          'financiada'  => $financiada,
          'tarjeta'     => $tarjeta,
          'CDO_PUBLICO' => $cdo,
          'flete'       => $flete,
          'accesorio'   => $accesorio,
          'descripcion' => $descripcion,
          'categoria'   => $categoria,
          'marca_id'    => $marca_id,
      );

      $resultado = $this->db->insert('modelos', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca plan por id
    //----------------------------------------------------------------------
    function consultarModeloRegistro($id){
         
      $sql    = "SELECT ext.id, ext.marca_id, ext.modelo, ext.CDO_PUBLICO as cdo, ext.accesorio, ext.descripcion, ext.categoria, ext.contado, ext.flete, ext.financiada, ext.tarjeta
                 FROM modelos ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "modelo"      =>    $row['modelo'],
            "contado"     =>    $row['contado'],
            "financiada"  =>    $row['financiada'],
            "tarjeta"     =>    $row['tarjeta'],
            "cdo"         =>    $row['cdo'],
            "flete"       =>    $row['flete'],
            "accesorio"   =>    $row['accesorio'],
            "descripcion" =>    $row['descripcion'],
            "categoria"   =>    $row['categoria'],
            "marca"       =>    $row['marca_id'],
            "id"          =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Modelo
    //----------------------------------------------------------------------
    function editarRegistroModelo($modelo,$contado,$financiada,$tarjeta,$cdo,$flete,$accesorio,$descripcion,$categoria,$marca_id,$id){
      $data = array(
          'modelo'      => $modelo,
          'contado'     => $contado,
          'financiada'  => $financiada,
          'tarjeta'     => $tarjeta,
          'CDO_PUBLICO' => $cdo,
          'flete'       => $flete,
          'accesorio'   => $accesorio,
          'descripcion' => $descripcion,
          'categoria'   => $categoria,
          'marca_id'    => $marca_id,
      );

      $this->db->where('id', $id);
      $this->db->update('modelos', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Modelo
    //---------------------------------------------------------------------- 
    function limpiarRegistroModelo($id){
      $sql = "SELECT * FROM modelos d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('modelos');
        return 1;
      }//if   
      else {
        return 2; // Este Modelo no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>