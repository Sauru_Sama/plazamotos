<?php
  Class Usuario extends CI_Model
  {
    //---------------------------------------------------------------------- 
    //  Obtiene listado de roles activos en el sistema
    //----------------------------------------------------------------------  
    function consultarRolBD(){

      $sql = "SELECT ro.nombre, ro.id
              FROM roles ro
              ORDER BY ro.nombre ASC";     

      $query = $this->db->query($sql);     

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 0;
        foreach ($query->result_array() as $reg){
          $v_resultado [$i]['id']    = $reg['id'];
          $v_resultado [$i]['valor'] = $reg['nombre'];
          $i++;
        }//foreach
      }//else        

      return($v_resultado);  //Se coloco la funcion json_encode que permite retornar el valor tipo array
      $query->free_result();
      $this->db->close(); //Cierre Manual de la Base de Datos.

    }//function  
    //---------------------------------------------------------------------- 
    //  Obtiene listado de Organizaciones en el sistema
    //----------------------------------------------------------------------  
    function consultarOrganizacionBD(){

      $sql = "SELECT org.nombre, org.id
              FROM organizaciones org
              ORDER BY org.nombre ASC";     

      $query = $this->db->query($sql);     

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 0;
        foreach ($query->result_array() as $reg){
          $v_resultado [$i]['id']    = $reg['id'];
          $v_resultado [$i]['valor'] = $reg['nombre'];
          $i++;
        }//foreach
      }//else        

      return($v_resultado);  //Se coloco la funcion json_encode que permite retornar el valor tipo array
      $query->free_result();
      $this->db->close(); //Cierre Manual de la Base de Datos.

    }//function 
    //----------------------------------------------------------------------
    //Listado de temas habilitados de acuerdo al proceso seleccionado
    //----------------------------------------------------------------------
    function consultaUsuario($nombre,$apellido,$indicador){
            
      $cadena = "";
      $sql    = "SELECT pe.id, pe.nombre, pe.apellido, pe.indicador
                 FROM personas pe
                 WHERE 1=1";
      
      if($nombre != "0"){
        $cadena.= " AND pe.nombre LIKE '%$nombre%'";
      }
      if($apellido != "0"){
        $cadena.= " AND pe.apellido LIKE '%$apellido%'";
      }
      if($indicador != "0"){
        $cadena.= " AND pe.indicador LIKE '%$indicador%'";
      }     
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['nombre'].'</td>';
          $lista_datos .= '<td>'.$row['apellido'].'</td>';
          $lista_datos .= '<td>'.$row['indicador'].'</td>';  
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    //Listado de temas habilitados de acuerdo al proceso seleccionado
    //----------------------------------------------------------------------
    function guardarUsuario($nombre,$apellido,$indicador,$clave){      
      $data = array(
          'nombre'          => $nombre,
          'apellido'        => $apellido,
          'indicador'       => $indicador,
          'clave'           => $clave,
      );

      $resultado = $this->db->insert('personas', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca usuario por id
    //----------------------------------------------------------------------
    function consultarUsuarioRegistro($id){
         
      $sql    = "SELECT pe.id, pe.nombre, pe.apellido, pe.indicador
                 FROM personas pe
                 WHERE pe.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "nombre"        =>    $row['nombre'],
            "apellido"      =>    $row['apellido'],
            "indicador"     =>    $row['indicador'],
            "id"            =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Usuario
    //----------------------------------------------------------------------
    function editarRegistroUsuario($nombre,$apellido,$indicador,$clave,$id){
      $data = array(
          'nombre'          => $nombre,
          'apellido'        => $apellido,
          'indicador'       => $indicador,
          'clave'           => $clave,
      );

      $this->db->where('id', $id);
      $this->db->update('personas', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Usuario
    //---------------------------------------------------------------------- 
    function limpiarRegistroUsuario($id){
      $sql = "SELECT * FROM personas d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('personas');
        return 1;
      }//if   
      else {
        return 2; // Este Usuario no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>