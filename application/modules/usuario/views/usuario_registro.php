<script>
  var baseurl = "<?php print base_url(); ?>";
  $(document).ready(function(){
    //------------------------------------------------------
    function registro_formulario(){
      v_nombre        = $("#v_nombre").val().trim().toUpperCase();
      v_apellido      = $("#v_apellido").val().trim().toUpperCase();
      v_indicador     = $("#v_indicador").val().trim().toUpperCase();   
      v_clave         = $("#v_clave").val().trim();    
      
      if ((v_clave == "") && (v_nombre == "") && (v_apellido == "") && (v_indicador == "")) {
        $("#v_nombre").addClass("rojo-requerido"); 
        $("#v_apellido").addClass("rojo-requerido");
        $("#v_indicador").addClass("rojo-requerido"); 
        $("#v_clave").addClass("rojo-requerido"); 
      }
      else if(v_nombre == ""){
        $("#v_nombre").addClass("rojo-requerido");  
      }
      else if(v_apellido == ""){
        $("#v_apellido").addClass("rojo-requerido");  
      }
      else if(v_indicador == ""){
        $("#v_indicador").addClass("rojo-requerido");  
      }
      else if(v_clave == ""){
        $("#v_clave").addClass("rojo-requerido");  
      }
      else {
        params = {
          'nombre': v_nombre,
          'apellido': v_apellido,
          'indicador': v_indicador,
          'clave': v_clave,
        };

        $.ajax({
          type: 'POST',
          url: baseurl+"usuario/c_usuario/guardarUsuario",
          data: params,
          success: function(data){                
            if(data == 1){
              exito_guardar();
              $("input[type='text']").val("");
            }          
            else{
              error_guardar();
            }
            
          }, //success
          error: function( jqXhr, textStatus, errorThrown )
          {
            console.log( textStatus+" = "+errorThrown );        
          }//error
        });//ajax
      }//else
    }//funcion     
    //------------------------------------------------------
    $("#b_usuario_guardar").click(function(){
      registro_formulario();
    });
    //------------------------------------------------------
    $("#b_usuario_limpiar").click(function(){
      $("input[type='text']").removeClass("rojo-requerido");
      $("select").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
    $("input[type='text']").change(function(){
      $("input[type='text']").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Usuarios / Registro
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-file-o"></i>&nbsp;&nbsp;Nuevo Registro</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="<?php echo base_url();?>/usuario/c_usuario">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-3">
                <label for="v_nombre">Nombre</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_nombre" placeholder="">
                </div>
              </div>

              <div class="col-md-3">
                <label for="v_apellido">Apellido</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_apellido" placeholder="">
                </div>
              </div>
               
              <div class="col-md-3">
                <label for="v_indicador">Indicador</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_indicador" placeholder="">
                </div>
              </div>  

              <div class="col-md-3">
                <label for="v_clave">Clave</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_clave" placeholder="">
                </div>
              </div>  

            </fieldset>           
            <!-- ///////////////////////////////////////////////////////// -->
              <div class="btn-group pull-right">               
                <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
                  <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
                </button>
                <a type="button" id="b_usuario_regresar" href="<?php echo site_url('usuario'); ?>" class="btn btn-default">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Regresar
                </a>
                <button id="b_usuario_guardar" type="button" class="btn btn-warning">
                  <span class="glyphicon glyphicon-save" aria-hidden="true"></span> Guardar
                </button> 
              </div>
            <!-- ///////////////////////////////////////////////////////// -->
          </form>
          <!-- ///////////////////////////////////////////////////////// -->
      </div>
      <!-- ///////////////////////////////////////////////////////// -->
      <div id="usuario_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->
    </div>
    <!-- ///////////////////////////////////////////////////////// -->
  </div>      
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->