<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_tarjeta extends MX_Controller {
function __construct(){
		parent::__construct();
		/* ------- helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./helper------ */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
	  	$this->form_validation->CI =& $this;
	  	/* ------- ./form_validation ------- */

	  	/* ------- Modelo --------- */
	  	$this->load->model('menus');
	  	$this->load->model('tarjeta');
	  	/* ------- ./Modelo ------- */
		
	}//function
//---------------------------------------------------------------------- 
	public function index(){
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('tarjeta/tarjeta_consulta');		
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function consultarTarjeta(){    
		$v_tarjeta = $this->input->post('tarjeta');
    	$consulta = $this->tarjeta->consultaTarjeta($v_tarjeta);         
    	$data["tabla_consulta"] = $consulta;	     

  		echo ($this->load->view('tarjeta/listado_tarjeta',$data));
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function cargarTarjetaRegistro(){    
   		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
   
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('tarjeta/tarjeta_registro');		
		$this->load->view('plantillas/footer');    	
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function editarTarjeta(){    
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
		/* -------  ------- */
		$id = $this->uri->segment(3);
    	$consultarTarjeta = $this->tarjeta->consultarTarjetaRegistro($id); 
    	$datos["datos_tarjeta"] = $consultarTarjeta;
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('tarjeta/tarjeta_editar_registro',$datos);		
		$this->load->view('plantillas/footer');    
	
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function guardarTarjeta(){    
		$v_tarjeta= $this->input->post('tarjeta');
    	echo $consulta = $this->tarjeta->guardarTarjeta($v_tarjeta);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function editarRegistroTarjeta(){
		$v_tarjeta= $this->input->post('tarjeta');
		$v_id= $this->input->post('id');    
    	echo $consulta = $this->tarjeta->editarRegistroTarjeta($v_tarjeta,$v_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function limpiarRegistroTarjeta($v_id){    
    	echo $consulta = $this->tarjeta->limpiarRegistroTarjeta($v_id);
	}//function	
	//----------------------------------------------------------------------
	function cerrarSesion(){
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	 	session_destroy();
	  	redirect('login/c_verifica_login', 'refresh');
	}	
//----------------------------------------------------------------------
}//class