<?php
  Class Tarjeta extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaTarjeta($tarjeta){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.tarjeta
                 FROM tarjetas ext
                 WHERE 1=1";
      
      if($tarjeta != "0"){
        $cadena.= " AND ext.tarjeta LIKE '%$tarjeta%'";
      }
            
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['tarjeta'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarTarjeta($tarjeta){      
      $data = array(
          'tarjeta'    => $tarjeta,
      );

      $resultado = $this->db->insert('tarjetas', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca tarjeta por id
    //----------------------------------------------------------------------
    function consultarTarjetaRegistro($id){
         
      $sql    = "SELECT ext.id, ext.tarjeta
                 FROM tarjetas ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "tarjeta" =>    $row['tarjeta'],
            "id"    =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Tarjeta
    //----------------------------------------------------------------------
    function editarRegistroTarjeta($tarjeta,$id){
      $data = array(
          'tarjeta'    => $tarjeta,
      );

      $this->db->where('id', $id);
      $this->db->update('tarjetas', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Tarjeta
    //---------------------------------------------------------------------- 
    function limpiarRegistroTarjeta($id){
      $sql = "SELECT * FROM tarjetas d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('tarjetas');
        return 1;
      }//if   
      else {
        return 2; // Este Tarjeta no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>