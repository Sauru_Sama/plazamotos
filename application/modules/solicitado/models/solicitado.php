<?php
  Class Solicitado extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaSolicitado($solicitado,$monto){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.solicitado, ext.monto
                 FROM solicitados ext
                 WHERE 1=1";
      
      if($solicitado != "0"){
        $cadena.= " AND ext.solicitado LIKE '%$solicitado%'";
      }
      if($monto != "0"){
        $cadena.= " AND ext.monto = '$monto'";
      }
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['solicitado'].'</td>';
          $lista_datos .= '<td>'.$row['monto'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarSolicitado($solicitado,$monto){      
      $data = array(
          'solicitado'    => $solicitado,
          'monto'        => $monto,
      );

      $resultado = $this->db->insert('solicitados', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca solicitado por id
    //----------------------------------------------------------------------
    function consultarSolicitadoRegistro($id){
         
      $sql    = "SELECT ext.id, ext.solicitado, ext.monto
                 FROM solicitados ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "solicitado"  =>    $row['solicitado'],
            "monto"      =>    $row['monto'],
            "id"         =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Solicitado
    //----------------------------------------------------------------------
    function editarRegistroSolicitado($solicitado,$monto,$id){
      $data = array(
          'solicitado'    => $solicitado,
          'monto'        => $monto,
      );

      $this->db->where('id', $id);
      $this->db->update('solicitados', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Solicitado
    //---------------------------------------------------------------------- 
    function limpiarRegistroSolicitado($id){
      $sql = "SELECT * FROM solicitados d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('solicitados');
        return 1;
      }//if   
      else {
        return 2; // Este Solicitado no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>