<script>
  var baseurl = "<?php print base_url(); ?>";
  $(document).ready(function(){
    //------------------------------------------------------
    function registro_formulario(){
      v_solicitado  = $("#v_solicitado").val().trim().toUpperCase();
      v_monto      = $("#v_monto").val().trim();
      
      if ((v_solicitado == "") && (v_monto == "")) {
        $("#v_solicitado").addClass("rojo-requerido"); 
        $("#v_monto").addClass("rojo-requerido"); 
      }
      else if(v_solicitado == ""){
        $("#v_solicitado").addClass("rojo-requerido");  
      }
      else if(v_monto == ""){
        $("#v_monto").addClass("rojo-requerido");  
      }
      else {
        params = {
          'solicitado': v_solicitado,
          'monto': v_monto,
        };

        $.ajax({
          type: 'POST',
          url: baseurl+"solicitado/c_solicitado/guardarSolicitado",
          data: params,
          success: function(data){                
            if(data == 1){
              exito_guardar();
              $("input[type='text']").val("");
            }          
            else{
              error_guardar();
            }
            
          }, //success
          error: function( jqXhr, textStatus, errorThrown )
          {
            console.log( textStatus+" = "+errorThrown );        
          }//error
        });//ajax
      }//else
    }//funcion     
    //------------------------------------------------------
    $("#b_usuario_guardar").click(function(){
      registro_formulario();
    });
    //------------------------------------------------------
    $("#b_usuario_limpiar").click(function(){
      $("input[type='text']").removeClass("rojo-requerido");
      $("select").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
    $("input[type='text']").change(function(){
      $("input[type='text']").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Solicitados / Registro
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-file-o"></i>&nbsp;&nbsp;Nuevo Registro</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="<?php echo base_url();?>/solicitado/c_solicitado">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_solicitado">Solicitado</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_solicitado" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_monto">Monto</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_monto" placeholder="">
                </div>
              </div>          

            </fieldset>           
            <!-- ///////////////////////////////////////////////////////// -->
              <div class="btn-group pull-right">               
                <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
                  <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
                </button>
                <a type="button" id="b_usuario_regresar" href="<?php echo site_url('solicitado'); ?>" class="btn btn-default">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Regresar
                </a>
                <button id="b_usuario_guardar" type="button" class="btn btn-warning">
                  <span class="glyphicon glyphicon-save" aria-hidden="true"></span> Guardar
                </button> 
              </div>
            <!-- ///////////////////////////////////////////////////////// -->
          </form>
          <!-- ///////////////////////////////////////////////////////// -->
      </div>
      <!-- ///////////////////////////////////////////////////////// -->
      <div id="solicitado_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->
    </div>
    <!-- ///////////////////////////////////////////////////////// -->
  </div>      
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->