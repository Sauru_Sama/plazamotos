<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_solicitado extends MX_Controller {
function __construct(){
		parent::__construct();
		/* ------- helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./helper------ */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
	  	$this->form_validation->CI =& $this;
	  	/* ------- ./form_validation ------- */

	  	/* ------- Modelo --------- */
	  	$this->load->model('menus');
	  	$this->load->model('solicitado');
	  	/* ------- ./Modelo ------- */
		
	}//function
//---------------------------------------------------------------------- 
	public function index(){
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('solicitado/solicitado_consulta');		
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function consultarSolicitado(){    
		$v_solicitado = $this->input->post('solicitado');
		$v_monto = $this->input->post('monto');
   		$consulta = $this->solicitado->consultaSolicitado($v_solicitado,$v_monto);         
    	$data["tabla_consulta"] = $consulta;	     

  		echo ($this->load->view('solicitado/listado_solicitado',$data));
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function cargarSolicitadoRegistro(){    
   		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
   
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('solicitado/solicitado_registro');		
		$this->load->view('plantillas/footer');    	
	}//function

	//---------------------------------------------------------------------- FUNCION
	public function editarSolicitado(){    
		// ------- Arreglo de Menú ------- 
		$arr_b_m = h_getMenu();
		// ------- ./Arreglo de Menú -------

		// ------- Carga la Librería Menu -------
		$this->load->library('Menu');
		// ------- ./Carga la Librería Menu -------
	
		// ------- Envía el arreglo de Menu a la Vista ------- 
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		// ------- ./Envía el arreglo de Menu a la Vista ------- 

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */
		/* -------  ------- */
		$id = $this->uri->segment(3);
    	$consultarSolicitado = $this->solicitado->consultarSolicitadoRegistro($id); 
    	$datos["datos_solicitado"] = $consultarSolicitado;
		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);		
		$this->load->view('solicitado/solicitado_editar_registro',$datos);		
		$this->load->view('plantillas/footer');    
	
	}//function
	//---------------------------------------------------------------------- FUNCION
	public function guardarSolicitado(){    
		$v_solicitado= $this->input->post('solicitado');
		$v_monto= $this->input->post('monto');
    	echo $consulta = $this->solicitado->guardarSolicitado($v_solicitado,$v_monto);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function editarRegistroSolicitado(){
		$v_solicitado= $this->input->post('solicitado');
		$v_monto= $this->input->post('monto');
		$v_id= $this->input->post('id');    
    	echo $consulta = $this->solicitado->editarRegistroSolicitado($v_solicitado,$v_monto,$v_id);
	}//function	
	//---------------------------------------------------------------------- FUNCION
	public function limpiarRegistroSolicitado($v_id){    
    	echo $consulta = $this->solicitado->limpiarRegistroSolicitado($v_id);
	}//function	
	//----------------------------------------------------------------------
	function cerrarSesion(){
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	  	session_destroy();
	  	redirect('login/c_verifica_login', 'refresh');
	}	
//----------------------------------------------------------------------
}//class