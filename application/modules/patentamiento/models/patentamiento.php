<?php
  Class Patentamiento extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaPatentamiento($patentamiento,$monto,$factor){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.patentamiento, ext.monto, ext.factor
                 FROM patentamientos ext
                 WHERE 1=1";
      
      if($patentamiento != "0"){
        $cadena.= " AND ext.patentamiento LIKE '%$patentamiento%'";
      }
      if($monto != "0"){
        $cadena.= " AND ext.monto = '$monto'";
      }
      if($factor != "0"){
        $cadena.= " AND ext.factor = '$factor'";
      }
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['patentamiento'].'</td>';
          $lista_datos .= '<td>'.$row['monto'].'</td>';
          $lista_datos .= '<td>'.$row['factor'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarPatentamiento($patentamiento,$monto,$factor){      
      $data = array(
          'patentamiento'    => $patentamiento,
          'monto'        => $monto,
          'factor'        => $factor,
      );

      $resultado = $this->db->insert('patentamientos', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca patentamiento por id
    //----------------------------------------------------------------------
    function consultarPatentamientoRegistro($id){
         
      $sql    = "SELECT ext.id, ext.patentamiento, ext.monto, ext.factor
                 FROM patentamientos ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "patentamiento"  =>    $row['patentamiento'],
            "monto"      =>    $row['monto'],
            "factor"      =>    $row['factor'],
            "id"         =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Patentamiento
    //----------------------------------------------------------------------
    function editarRegistroPatentamiento($patentamiento,$monto,$factor,$id){
      $data = array(
          'patentamiento'    => $patentamiento,
          'monto'        => $monto,
          'factor'        => $factor,
      );

      $this->db->where('id', $id);
      $this->db->update('patentamientos', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Patentamiento
    //---------------------------------------------------------------------- 
    function limpiarRegistroPatentamiento($id){
      $sql = "SELECT * FROM patentamientos d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('patentamientos');
        return 1;
      }//if   
      else {
        return 2; // Este Patentamiento no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>