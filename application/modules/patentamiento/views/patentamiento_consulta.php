<script>
  $(document).ready(function(){
    var baseurl = "<?php print base_url(); ?>";
    //------------------------------------------------------
    $("#b_patentamiento_limpiar").on('click', function() {
      $("#patentamiento_consulta_listado").empty();
    });

    function consulta_formulario(){
      v_patentamiento    = $("#v_patentamiento").val().trim().toUpperCase();
      v_monto  = $("#v_monto").val().trim();
      v_factor  = $("#v_factor").val().trim();
      
      if(v_patentamiento == ""){
        v_patentamiento = 0;
      }
      if(v_monto == ""){
        v_monto = 0;
      }
      if(v_factor == ""){
        v_factor = 0;
      }

      var params = {
        'patentamiento':v_patentamiento,
        'monto':v_monto,
        'factor':v_factor,
      };

      $.ajax({
        type: 'POST',
        url: baseurl+"patentamiento/c_patentamiento/consultarPatentamiento",
        data: params,
        success: function(data){              
          $("#patentamiento_consulta_listado").html(data);
          $('#tabla_patentamiento_listado').dataTable( { 
            "sPaginationType": "full_numbers",
            "scrollY":        "255px",
            "scrollCollapse": false,   
            "bAutoWidth": false,
            "bDestroy": true,  
            "responsive": true,                              
            "language": {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
                "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "&Uacute;ltimo",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              },
            },   
            "order": [[ 0, "asc" ]],
            "columnDefs": [
              {
                "targets": [ 3 ],
                "visible": false,
                "searchable": false
              },
              {
                "targets": [ 4 ],
                "searchable": false,
                "orderable": false
              }
            ],
          });    
          var table = $("#tabla_patentamiento_listado").DataTable(); // importante!
          //  Escucha al botón, toma el valor de la columna con el id en la fila donde esté y la pasa al enlace y redirige.
          $("#tabla_patentamiento_listado tbody").on("click", ".btn-editar", function (e) {
          var data   = table.row( $(this).parents("tr") ).data();                                                  
          var vj_patentamiento_id    = data[3].trim();
          newPage = baseurl+"patentamiento/editar/"+vj_patentamiento_id;
          window.location.href = newPage;              
          } );
          //  Escucha al botón, toma el valor de la columna con el id en la fila donde esté y la pasa al enlace y redirige.
          $("#tabla_patentamiento_listado tbody").on("click", ".btn-eliminar", function (e) {
          var data   = table.row( $(this).parents("tr") ).data();                                                  
          var vj_patentamiento_id    = data[3].trim();
          var esto = $(this).parents("tr");      
          
          sweetAlert({
              title: "¿Desea eliminar el Patentamiento?",
              text: "Esta acción no se puede deshacer",
              type: "warning",
              showCancelButton: true,
              cancelButtonText: "No, no gracias.",
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Si, estoy seguro.",
              closeOnConfirm: false
            },
            function(){
              $.ajax({
                type: 'POST',
                url: baseurl+"patentamiento/c_patentamiento/limpiarRegistroPatentamiento/"+vj_patentamiento_id,
                type: 'json',
                success: function(data){                
                  if(data == 1){
                    sweetAlert("Exito.",'Se eliminaron satisfacoriamente los datos.', 'success');
                  }          
                  else{
                    sweetAlert('Disculpe.', 'Hubo problemas al intentar eliminar los datos', 'error');
                  }                              
                }, //success
                error: function( jqXhr, textStatus, errorThrown ){
                console.log( textStatus+" = "+errorThrown);        
                }//error
              });//ajax 
              table
                .row( esto )
                .remove()
                .draw();        
            });    
          } );

          $( $.fn.dataTable.tables( true ) ).DataTable().columns.adjust();              
        }, //success
        error: function( jqXhr, textStatus, errorThrown ){
        console.log( textStatus+" = "+errorThrown);        
        }//error
      });//ajax
    }//funcion     
    //------------------------------------------------------
    $("#b_usuario_buscar").click(function(){
      consulta_formulario();
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  <div id="confirmar_deshabilitar" title="Confirmaci&oacute;n"></div>
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Patentamientos / Consultar
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-search"></i>&nbsp;&nbsp;Consulta</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="c_patentamiento/cargarPatentamientoRegistro">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_patentamiento">Patentamiento</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_patentamiento" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_monto">Monto</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_monto" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_factor">Factor</label>
                <div class="col-md-9 pull-right">
                  <input type="text" class="form-control input-sm" id="v_factor" placeholder="">
                </div>
              </div>

            </fieldset>

            <!-- ///////////////////////////////////////////////////////// -->
            <div class="btn-group pull-right">
              <button id="b_usuario_buscar" type="button" class="btn btn-default">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar
              </button>
              <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
                <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
              </button>
              <a type="button" id="b_usuario_agregar" href="<?php echo site_url('patentamiento/nuevo'); ?>" class="btn btn-warning">
                <span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> Agregar
              </a>      
            </div>
            <!-- ///////////////////////////////////////////////////////// -->
            
          </form>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div id="patentamiento_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->

    </div>

  </div>
  
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div id="reporte_detalle_vista_previa"></div>       
<!-- ///////////////////////////////////////////////////////// -->