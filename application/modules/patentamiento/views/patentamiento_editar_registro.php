<script>
  var baseurl = "<?php print base_url(); ?>";
  $(document).ready(function(){
    $("#v_patentamiento").val(<?php echo '"'.$datos_patentamiento["patentamiento"].'"'; ?>);
    $("#v_monto").val(<?php echo '"'.$datos_patentamiento["monto"].'"'; ?>);
    $("#v_factor").val(<?php echo '"'.$datos_patentamiento["factor"].'"'; ?>);
    $("#v_id").prop('value', <?php echo $datos_patentamiento["id"]; ?>);
    var v_patentamiento_id = <?php echo $datos_patentamiento["id"]; ?>;
    //------------------------------------------------------
    function edita_formulario(){
      v_patentamiento     = $("#v_patentamiento").val().trim().toUpperCase();
      v_monto         = $("#v_monto").val().trim();
      v_factor         = $("#v_factor").val().trim();
      v_id            = $("#v_id").val();      
      
      if ((v_patentamiento == "") && (v_monto == "") && (v_factor == "")) {
        $("#v_patentamiento").addClass("rojo-requerido"); 
        $("#v_monto").addClass("rojo-requerido"); 
        $("#v_factor").addClass("rojo-requerido"); 
      }
      else if(v_patentamiento == ""){
        $("#v_patentamiento").addClass("rojo-requerido");  
      }
      else if(v_monto == ""){
        $("#v_monto").addClass("rojo-requerido");  
      }
      else if(v_factor == ""){
        $("#v_factor").addClass("rojo-requerido");  
      }
      else {
        params = {
          'patentamiento': v_patentamiento,
          'monto': v_monto,
          'factor': v_factor,
          'id' : v_id,
        };

        $.ajax({
          type: 'POST',
          url: baseurl+"patentamiento/c_patentamiento/editarRegistroPatentamiento",
          data: params,
          success: function(data){                
            if(data == 1){
              exito_guardar();
            }          
            else{
              error_guardar();
            }            
          }, //success
          error: function( jqXhr, textStatus, errorThrown ){
          console.log( textStatus+" = "+errorThrown);        
          }//error
        });//ajax
      }//else
    }//funcion    
    //------------------------------------------------------
    function eliminar_patentamiento(v_patentamiento){
      sweetAlert({
                title: "¿Desea eliminar el Patentamiento?",
                text: "Esta acción no se puede deshacer",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No, no gracias.",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, estoy seguro.",
                closeOnConfirm: false
              },
              function(isConfirm){
                 $.ajax({
                    type: 'POST',
                    url: baseurl+"patentamiento/c_patentamiento/limpiarRegistroPatentamiento/"+v_patentamiento_id,
                    type:'json',
                    success: function(data){                
                      if(data == 1){
                        sweetAlert("Exito.",'Se eliminaron satisfactoriamente los datos.', 'success');  
                        if (isConfirm) {
                            window.location.href = "<?php echo site_url('patentamiento'); ?>";
                        };        
                      }else{
                        sweetAlert('Disculpe.', 'Hubo problemas al intentar eliminar los datos', 'error');
                      }                              
                    }, //success
                    error: function( jqXhr, textStatus, errorThrown ){
                    console.log( textStatus+" = "+errorThrown);        
                    }//error
                  });//ajax       
              });  
    }//function
    //------------------------------------------------------ 
    //------------------------------------------------------
    $("#b_usuario_guardar").click(function(){
      edita_formulario();
    });
    //------------------------------------------------------
    $("#b_rol_eliminar").click(function(){
      eliminar_patentamiento(v_patentamiento_id);
    });
    //------------------------------------------------------
    $("#b_usuario_limpiar").click(function(){
      $("input[type='text']").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
    $("input[type='text']").change(function(){
      $("input[type='text']").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
    $("select").change(function(){
      $("select").removeClass("rojo-requerido");
    });
    //------------------------------------------------------
  });
</script>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
  <div class="col-xs-12 col-sm-12 col-md-12">
  
    <div class="panel panel-default">
     
      <div class="panel-heading">
        Patentamientos / Registro
        <div class="rojo-asterisco float-right">
          <b><i class="fa fa-file-o"></i>&nbsp;&nbsp;Editar Registro</b>
        </div>
      </div>

      <!-- ///////////////////////////////////////////////////////// -->
      <div class="panel-body fuente_mediana">

          <form role="form" method="POST" action="<?php echo base_url();?>/patentamiento/c_patentamiento">
            <!-- ///////////////////////////////////////////////////////// -->
            <fieldset class="form-group">
              
              <div class="col-md-4">
                <label for="v_patentamiento">Patentamiento</label>
                <div class="col-md-9 pull-right form-group">
                  <input type="text" class="form-control input-sm" id="v_patentamiento" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_monto">Monto</label>
                <div class="col-md-9 pull-right form-group">
                  <input type="text" class="form-control input-sm" id="v_monto" placeholder="">
                </div>
              </div>

              <div class="col-md-4">
                <label for="v_factor">Factor</label>
                <div class="col-md-9 pull-right form-group">
                  <input type="text" class="form-control input-sm" id="v_factor" placeholder="">
                </div>
              </div>
                       
            </fieldset>           
            <div class="spacer10"></div>
             <!-- ///////////////////////////////////////////////////////// -->       
            <input type="hidden" id="v_id" name="v_id">
          </form>

          <div class="spacer20"></div>
          <div class="btn-group pull-right">               
            <button id="b_usuario_limpiar" type="reset" class="btn btn-default">
              <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Limpiar
            </button>
            <a type="button" id="b_usuario_nuevo" href="<?php echo site_url('patentamiento/nuevo'); ?>" class="btn btn-default">
              <span class="glyphicon glyphicon glyphicon-file" aria-hidden="true"></span> Nuevo
            </a>
            <a type="button" id="b_usuario_regresar" href="<?php echo site_url('patentamiento'); ?>" class="btn btn-default">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Regresar
            </a>
            <button id="b_rol_eliminar" type="reset" class="btn btn-danger">
              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Eliminar
            </button>
            <button id="b_usuario_guardar" type="button" class="btn btn-warning">
              <span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> Guardar
            </button> 
          </div>

      </div>
      <!-- ///////////////////////////////////////////////////////// -->
      <div id="patentamiento_consulta_listado" class="margen_completo_p fuente_mediana"></div>      
      <!-- ///////////////////////////////////////////////////////// -->
    </div>
    <!-- ///////////////////////////////////////////////////////// -->
    
    <!-- ///////////////////////////////////////////////////////// -->
  </div>
    
  
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="reporte_detalle_vista_previa"></div>       
<!-- ///////////////////////////////////////////////////////// -->