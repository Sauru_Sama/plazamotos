<?php
  Class Accesorio extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaAccesorio($accesorio,$monto){
            
      $cadena = "";
      $sql    = "SELECT acc.id, acc.accesorio, acc.monto
                 FROM accesorios acc
                 WHERE 1=1";
      
      if($accesorio != "0"){
        $cadena.= " AND acc.accesorio LIKE '%$accesorio%'";
      }
      if($monto != "0"){
        $cadena.= " AND acc.monto = '$monto'";
      }
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['accesorio'].'</td>';
          $lista_datos .= '<td>'.$row['monto'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //----------------------------------------------------------------------
    function guardarAccesorio($accesorio,$monto){      
      $data = array(
          'accesorio'    => $accesorio,
          'monto'        => $monto,
      );

      $resultado = $this->db->insert('accesorios', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca accesorio por id
    //----------------------------------------------------------------------
    function consultarAccesorioRegistro($id){
         
      $sql    = "SELECT acc.id, acc.accesorio, acc.monto
                 FROM accesorios acc
                 WHERE acc.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "accesorio"  =>    $row['accesorio'],
            "monto"      =>    $row['monto'],
            "id"         =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Accesorio
    //----------------------------------------------------------------------
    function editarRegistroAccesorio($accesorio,$monto,$id){
      $data = array(
          'accesorio'    => $accesorio,
          'monto'        => $monto,
      );

      $this->db->where('id', $id);
      $this->db->update('accesorios', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Accesorio
    //---------------------------------------------------------------------- 
    function limpiarRegistroAccesorio($id){
      $sql = "SELECT * FROM accesorios d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('accesorios');
        return 1;
      }//if   
      else {
        return 2; // Este Accesorio no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>