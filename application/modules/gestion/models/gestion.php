<?php
  Class Gestion extends CI_Model
  {
    //----------------------------------------------------------------------
    function consultaGestion($gestion,$monto,$operacion_id){
            
      $cadena = "";
      $sql    = "SELECT ext.id, ext.gestion, ext.monto, ext.operacion_id, ope.operacion
                 FROM gestiones ext, operaciones ope
                 WHERE ext.operacion_id = ope.id";
      
      if($gestion != "0"){
        $cadena.= " AND ext.gestion LIKE '%$gestion%'";
      }
      if($monto != "0"){
        $cadena.= " AND ext.monto = '$monto'";
      }
      if($operacion_id != "0"){
        $cadena.= " AND ext.operacion_id = $operacion_id";
      }
      
      //Se incluye el orden a listar los datos
     
      $sql.= $cadena;

      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 1;
        $lista_datos = '';
                  
        foreach ($query->result_array() as $row)
        {
          $lista_datos .= '<tr>';                    
          $lista_datos .= '<td>'.$row['gestion'].'</td>';
          $lista_datos .= '<td>'.$row['monto'].'</td>';
          $lista_datos .= '<td>'.$row['operacion'].'</td>';
          $lista_datos .= '<td>'.$row['id'].'</td>';       
          $lista_datos .= '<td><button id="btn-editar-usuario" class="btn btn-sm btn-warning btn-editar" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></button>
                          &nbsp&nbsp<button id="btn-eliminar-usuario" class="btn btn-sm btn-danger btn-eliminar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times"></i></button></td>';
          $lista_datos .= '</tr>';          
        };
        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function    
    //---------------------------------------------------------------------- 
    //  Obtiene listado de operaciones
    //----------------------------------------------------------------------  
    function consultarOperacion(){

      $sql = "SELECT ro.operacion, ro.id
              FROM operaciones ro
              ORDER BY ro.operacion ASC";     

      $query = $this->db->query($sql);     

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $i = 0;
        foreach ($query->result_array() as $reg){
          $v_resultado [$i]['id']    = $reg['id'];
          $v_resultado [$i]['valor'] = $reg['operacion'];
          $i++;
        }//foreach
      }//else        

      return($v_resultado);  
      $query->free_result();
      $this->db->close(); 

    }//function  
    //----------------------------------------------------------------------
    function guardarGestion($gestion,$monto,$operacion_id){      
      $data = array(
          'gestion'    => $gestion,
          'monto'        => $monto,
          'operacion_id'        => $operacion_id,
      );

      $resultado = $this->db->insert('gestiones', $data); 
      $insert_id = $this->db->insert_id();

      return 1;     
    }//function 
    //----------------------------------------------------------------------
    //Busca gestion por id
    //----------------------------------------------------------------------
    function consultarGestionRegistro($id){
         
      $sql    = "SELECT ext.id, ext.gestion, ext.monto, ext.operacion_id
                 FROM gestiones ext
                 WHERE ext.id = $id";
      
      //Se incluye el orden a listar los datos
      $query = $this->db->query($sql);   

      if ($query->num_rows() == 0){
        return 1;
      }//if   
      else{
        $lista_datos = "";         
        foreach ($query->result_array() as $row)
        {           
          $lista_datos = array(
            "gestion"  =>    $row['gestion'],
            "monto"      =>    $row['monto'],
            "operacion"      =>    $row['operacion_id'],
            "id"         =>    $row['id']
          );        
        };        

        $query->free_result();
        return ($lista_datos);
      }//else        

    }//function  
    //----------------------------------------------------------------------
    // Edita el Gestion
    //----------------------------------------------------------------------
    function editarRegistroGestion($gestion,$monto,$operacion_id,$id){
      $data = array(
          'gestion'    => $gestion,
          'monto'        => $monto,
          'operacion_id'        => $operacion_id,
      );

      $this->db->where('id', $id);
      $this->db->update('gestiones', $data);
      return 1;

    }//function 
    //---------------------------------------------------------------------- 
    // Elimina el Gestion
    //---------------------------------------------------------------------- 
    function limpiarRegistroGestion($id){
      $sql = "SELECT * FROM gestiones d WHERE d.id = $id";
      $query = $this->db->query($sql);   
      if ($query->num_rows() > 0){
        $this->db->where('id',$id);
        $this->db->delete('gestiones');
        return 1;
      }//if   
      else {
        return 2; // Este Gestion no puede ser eliminado si tienen información relacionada
      }
    }//function
    //---------------------------------------------------------------------- 
  }//class
?>