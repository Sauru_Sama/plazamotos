<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_calculadora extends MX_Controller {
	function __construct(){
		parent::__construct();
		/* ------- h_plantilla_helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./h_plantilla_helper ------- */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
    $this->form_validation->CI =& $this;
    /* ------- ./form_validation ------- */

    /* ------- Modelos ------- */
    $this->load->model('menus');
    $this->load->model('Calculadora');

		//$this->output->enable_profiler(TRUE);
	}//function
//----------------------------------------------------------------------
	function index(){

		/* ------- Arreglo de Menú ------- */
		$arr_b_m = h_getMenu();
		/* ------- ./Arreglo de Menú ------- */

		/* ------- Carga la Librería Menu ------- */
		$this->load->library('Menu');
		/* ------- ./Carga la Librería Menu ------- */
	
		/* ------- Envía el arreglo de Menu a la Vista ------- */
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		/* ------- ./Envía el arreglo de Menu a la Vista ------- */

		/* ------- Título de Página y Subtitulo ------- */
		$data['el_titulo'] = 'Plaza Motos';
		/* ------- ./Título de Página y Subtitulo ------- */

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_2_plazamotos_10'] = h_img_2_plazamotos_10();

		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header_calculadora',$data);
		$this->load->view('calculadora/calculadora');
		$this->load->view('calculadora/modal-cuentas-plaza-motos');
		$this->load->view('calculadora/modal-numeros-utiles');
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function
//----------------------------------------------------------------------
	public function consultarOperaciones(){        
		$query = $this->input->get('q'); 
   	$consulta = $this->Calculadora->consultarOperaciones($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
//----------------------------------------------------------------------
	public function consultarMarcas(){ 
		$query = $this->input->get('q');
   	$consulta = $this->Calculadora->consultarMarcas($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
//----------------------------------------------------------------------
	public function consultarModelos(){ 
		$query = $this->input->get('q');        
   	$marcaId = $this->input->get('marcaId');
   	$consulta = $this->Calculadora->consultarModelos($query,$marcaId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarModelosPrecios(){ 
   	$modeloId = $this->input->get('modeloId');
   	$operacionId = $this->input->get('operacion');
   	$consulta = $this->Calculadora->consultarModelosPrecios($modeloId,$operacionId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarGestiones(){ 
		$query = $this->input->get('q');   
		$operacionId = $this->input->get('operacionId');        
   	$consulta = $this->Calculadora->consultarGestiones($query,$operacionId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarGestionesPrecios(){ 
   	$gestionId = $this->input->get('gestionId');
   	$consulta = $this->Calculadora->consultarGestionesPrecios($gestionId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarPatentamientos(){ 
		$query = $this->input->get('q');
   	$consulta = $this->Calculadora->consultarPatentamientos($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//
	//----------------------------------------------------------------------
	public function consultarPatentamientosPrecios(){ 
   	$patentamientoId = $this->input->get('patentamientoId');
   	$consulta = $this->Calculadora->consultarPatentamientosPrecios($patentamientoId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarAccesorios(){ 
		$query = $this->input->get('q');
   	$consulta = $this->Calculadora->consultarAccesorios($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarAccesoriosPrecios(){ 
   	$accesorioId = $this->input->get('accesorioId');
   	$consulta = $this->Calculadora->consultarAccesoriosPrecios($accesorioId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarGarantias(){ 
		$query = $this->input->get('q');
   	$consulta = $this->Calculadora->consultarGarantias($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarGarantiasPrecios(){ 
   	$garantiaId = $this->input->get('garantiaId');
   	$consulta = $this->Calculadora->consultarGarantiasPrecios($garantiaId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarExtras(){ 
		$query = $this->input->get('q');
   	$consulta = $this->Calculadora->consultarExtras($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarExtrasPrecios(){ 
   	$extraId = $this->input->get('extraId');
   	$consulta = $this->Calculadora->consultarExtrasPrecios($extraId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarSolicitados(){ 
		$query = $this->input->get('q');
   	$consulta = $this->Calculadora->consultarSolicitados($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarSolicitadosPrecios(){ 
   	$solicitadoId = $this->input->get('solicitadoId');
   	$consulta = $this->Calculadora->consultarSolicitadosPrecios($solicitadoId);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarTarjetas(){ 
		$query = $this->input->get('q');
   	$consulta = $this->Calculadora->consultarTarjetas($query);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarCuotas(){ 
		$query = $this->input->get('q');      
   	$tarjeta = $this->input->get('tarjeta');
   	$consulta = $this->Calculadora->consultarCuotas($query,$tarjeta);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	public function consultarCuotasCoeficientes(){ 
   	$cuota = $this->input->get('cuota');
   	$consulta = $this->Calculadora->consultarCuotasCoeficientes($cuota);         

		$this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($consulta));
	}//function
	//----------------------------------------------------------------------
	function cerrarSesion(){
    //----------------------------------
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	  session_destroy();
	  redirect('login/c_verifica_login', 'refresh');
	}
}//class