<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div class="table-responsive col-xs-12 col-sm-12 col-md-4">
	<table class="table table-bordered">
		<thead>
			<colgroup span="1"></colgroup>
			<tr class="tableHead">
			    <th colspan="3" scope="colgroup" class="cabecera-tabla"><center><b>Propuesta</b></center></th>
			</tr>
		</thead>

  	<tbody>
    	<tr>
			<td class="td white-font"><center>Operaci&oacute;n</center></td>
			<td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
				<center>
					<select id="sel_operacion" class="select2 s-width"></select>
				</center>
			</td>
			<td></td>
		</tr>

		<tr>
			<td class="td white-font"><center>Marca</center></td>
    		<td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
	    		<center>
		    		<select id="sel_marca" class="select2 s-width"></select>
		    	</center>
	    	</td>
	    	<td rowspan="2" style="vertical-align: middle;"><center><input id="inp_segun_modelo_operacion" class="tableHeadersRightSmaller form-control suma_1" type="text" style="font-size: 16px;" placeholder="0"></input></center></td>
		</tr>

		<tr>
			<td class="td white-font"><center>Modelo</center></td>
	    <td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
	    	<center>
		    	<select id="sel_modelo" class="select2 s-width"></select>
	    	</center>
	    </td>
		</tr>

		<tr>
			<td class="td white-font"><center>Categoria</center></td>
	    	<td><center><input id="inp_categoria" class="tableHeadersRightSmaller form-control" type="text" value=""></input></center></td>
	    	<td></td>
		</tr>

		<tr>
			<td rowspan="3" class="td white-font" style="vertical-align: middle;"><center>Gastos de<br>Gesti&oacute;n</center></td>
	    <td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
	    	<center>
		    	<select id="sel_gestion" class="select2 s-width"></select>
		    </center>
	    </td>
	    <td><center><input id="inp_gestion" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>
		<tr>
			<td class="white-font"><center>Retenci&oacute;n Crediticia</center></td>
			<td class="white-font"><center><input id="inp_retencion_crediticia" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>
		<tr>
			<td class="white-font"><center>Flete de Operaci&oacute;n</center></td>
			<td class="white-font"><center><input id="inp_flete_operacion" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>

		<tr>
			<td class="td white-font"><center>Patentamiento</center></td>
		    <td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
		    	<center>
			    	<select id="sel_patentamiento" class="select2 s-width"></select>
		    	</center>
		    </td>
		    <td><center><input id="inp_patentamiento" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>

		<tr>
			<td rowspan="2" class="td white-font" style="vertical-align: middle;"><center>Accesorios</center></td>
		    <td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
		    	<center>
			    	<select id="sel_accesorios" class="select2 s-width"></select>
		    	</center>
		    </td>
		    <td><center><input id="inp_accesorios" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>
		<tr>
			<td class="white-font"><center><b>Accesorio de Operaci&oacute;n</b></center></td>
			<td><center><input id="inp_accesorio_operacion" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>

		<tr>
			<td rowspan="2" class="td white-font" style="vertical-align: middle;"><center>Garant&iacute;a y<br>Sellado</center></td>
		    <td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
		    	<center>
			    	<select id="sel_garantias" class="select2 s-width"></select>
		    	</center>
		    </td>
		    <td><center><input id="inp_sellado" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>
		<tr>
			<td class="white-font"><center>Sellado x 2 Alta Gama</center></td>
			<td><center><input id="inp_sellado_2" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>

		<tr>
			<td class="td white-font"><center>Extras</center></td>
		    <td style="border: 2px solid #FFF !important; background-color: #c64c4d !important;">
		    	<center>
			    	<select id="sel_extras" class="select2 s-width"></select>
		    	</center>
		    </td>
		    <td><center><input id="inp_extra" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>

		<tr>
			<td rowspan="2" class="td white-font" style="vertical-align: middle;"><center>Otros</center></td>
			<td class="white-font"><center>FLETE/VERIFICACI&Oacute;N/08</center></td>
			<td><center><input id="inp_flete_verificacion" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>			
		</tr>
		<tr>
			<td class="white-font"><center>Accesorio Extra Manual</center></td>
			<td><center><input id="inp_accesorio_extra_manual" class="tableHeadersRightSmaller form-control suma_1" type="text" placeholder="0"></input></center></td>
		</tr>

		<tr>
    		<td style="vertical-align: middle; background-color: #000 !important; color: white !important; font-size: 18px !important;"><center><b>Total</b></center></td>
    		<td style="background-color: #000 !important;"></td>
    		<td style="background-color: #000 !important;"><center><input id="inp_total_1" class="tableHeadersRightSmaller form-control" style="font-size: 18px !important; color: #FFF !important;" type="text" value="0"></input></center></td>
    	</tr>
    </tbody>
	</table>
</div>

<!-- -->

<div class="table-responsive col-xs-12 col-sm-12 col-md-6">
	<table class="table table-bordered">
		<thead>
			<colgroup span="1"></colgroup>
			<tr class="tableHead">
			    <th colspan="8" scope="colgroup" class="cabecera-tabla"><center><b>Medios de Pago</b></center></th>
			</tr>
		</thead>

  		<tbody>
	    	<tr>
	    		<td style="width: 12em;"></td>
	    		<td></td>
	    		<td style="width: 12em;"></td>
	    		<td style="width: 12em;"></td>
	    		<td class="white-font"><center>Cr&eacute;dito</center></td>
	    		<td class="white-font"><center>Retenci&oacute;n</center></td>
	    	</tr>

	    	<tr>
	    		<td class="white-font" style="background-color: #F7F7F8 !important;"><center>Solicitado</center></td>
	    		<td style="background-color: #F7F7F8 !important;"><center><input id="inp_manual_s" class="manual tableHeadersRightSmaller form-control suma_2" type="text" placeholder="0"></input></center></td>
	    		<td class="white-font" rowspan="2" style="vertical-align: middle; background-color: #c64c4d !important; border: 2px solid #FFF !important;">
	    			<center>
		    			<select id="sel_medio_pago" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #F7F7F8 !important;"><center><input id="inp_solicitado_s" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<td style="background-color: #F7F7F8 !important;"><center><input id="inp_credito_s" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<td style="background-color: #F7F7F8 !important;"><center><input id="inp_retencion_s" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>
	    	<tr>
	    		<td class="white-font" style="background-color: #999999 !important;"><center>Otorgado</center></td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_manual_o" class="manual tableHeadersRightSmaller form-control suma_2" type="text" placeholder="0"></input></center></td>  		
	    		<td style="background-color: #999999 !important;"><center><input id="inp_otorgado_o" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_credito_o" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_retencion_o" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td class="white-font"><center>Entrega</center></td>
	    		<td><center><input id="inp_manual_entrega" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td class="white-font" colspan="2" style="vertical-align: middle;"><center>Cálculo Disponible</center></td>
	    		<td class="white-font" style="vertical-align: middle;"><center>Disponible</center></td>
	    		<td class="white-font" style="vertical-align: middle;"><center>Utilizable</center></td>
	    	</tr>

	    	<tr>
	    		<td class="white-font"><center>Pagar&eacute;</center></td>
	    		<td><center><input id="inp_manual_pagare" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_1" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_1" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #F7F7F8 !important; border: 2px solid #000 !important;"><center><input id="inp_manual_disponible" class="tableHeadersRightSmaller form-control" type="text" placeholder="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_1_coeficiente"></input>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;"><center><input id="inp_utilizable" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td style="font-weight: bold;"><center>Moto Usada</center></td>
	    		<td><center><input id="inp_manual_moto_usada" class="tableHeadersRightSmaller form-control tc suma_2" style="color: #000 !important;" type="text" placeholder="0"></input></center></td>
	    		<td></td>
	    		<td></td>
	    		<td></td>
	    		<td></td>
	    	</tr>

	    	<tr>
	    		<td colspan="2" style="background-color: #000 !important; color: #FFF !important; font-weight: bold !important;"><center>Tarjetas de Cr&eacute;dito</center></td>
	    		<td style="background-color: #000 !important; color: #FFF !important; font-weight: bold !important;"><center>Tarjeta</center></td>
	    		<td style="background-color: #000 !important; color: #FFF !important; font-weight: bold !important;"><center>Cuotas</center></td>
	    		<td style="background-color: #000 !important; color: #FFF !important; font-weight: bold !important;"><center>Cup&oacute;n</center></td>
	    		<td style="background-color: #000 !important; color: #FFF !important; font-weight: bold !important;"><center>Cuota</center></td>
	    	</tr>

	    	<tr>
	    		<td class="white-font" rowspan="9" style="vertical-align: middle; background-color: #F7F7F8 !important;"><?=img($img_plazamotos_10) ?></td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_tc_2" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_2" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_2" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cupon_2" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_2_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_2"></input>
	    		<input type="hidden" id="inp_2_x" class="suma_x" value="0"></input>
	    		<input type="hidden" id="inp_2_y" class="suma_y" value="0"></input>
	    		<input type="hidden" id="inp_plan_2"></input>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cuota_2" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>
	    	
	    	<tr>
	    		<td><center><input id="inp_tc_3" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_3" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_3" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td><center><input id="inp_cupon_3" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_3_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_3"></input>
	    		<input type="hidden" id="inp_3_x" class="suma_x" value="0"></input>
	    		<input type="hidden" id="inp_3_y" class="suma_y" value="0"></input>
	    		<input type="hidden" id="inp_plan_3"></input>
	    		<td><center><input id="inp_cuota_3" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_tc_4" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_4" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_4" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cupon_4" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_4_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_4"></input>
	    		<input type="hidden" id="inp_4_x" class="suma_x" value="0"></input>
	    		<input type="hidden" id="inp_4_y" class="suma_y" value="0"></input>
	    		<input type="hidden" id="inp_plan_4"></input>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cuota_4" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td><center><input id="inp_tc_5" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_5" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_5" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td><center><input id="inp_cupon_5" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_5_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_5"></input>
	    		<input type="hidden" id="inp_5_x" class="suma_x" value="0"></input>
	    		<input type="hidden" id="inp_5_y" class="suma_y" value="0"></input>
	    		<input type="hidden" id="inp_plan_5"></input>
	    		<td><center><input id="inp_cuota_5" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_tc_6" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_6" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_6" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cupon_6" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_6_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_6"></input>
	    		<input type="hidden" id="inp_6_x" class="suma_x" value="0"></input>
	    		<input type="hidden" id="inp_6_y" class="suma_y" value="0"></input>
	    		<input type="hidden" id="inp_plan_6"></input>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cuota_6" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td><center><input id="inp_tc_7" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_7" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_7" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td><center><input id="inp_cupon_7" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_7_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_7"></input>
	    		<input type="hidden" id="inp_7_x" class="suma_x" value="0"></input>
	    		<input type="hidden" id="inp_7_y" class="suma_y" value="0"></input>
	    		<input type="hidden" id="inp_plan_7"></input>
	    		<td><center><input id="inp_cuota_7" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_tc_8" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_8" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_8" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cupon_8" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_8_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_8"></input>
	    		<input type="hidden" id="inp_8_x" class="suma_x" value="0" value="0" value="0"></input>
	    		<input type="hidden" id="inp_8_y" class="suma_y" value="0" value="0" value="0"></input>
	    		<input type="hidden" id="inp_plan_8"></input>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cuota_8" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td><center><input id="inp_tc_9" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_9" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #C1C1C1 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_9" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td><center><input id="inp_cupon_9" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_9_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_9"></input>
	    		<input type="hidden" id="inp_9_x" class="suma_x" value="0" value="0"></input>
	    		<input type="hidden" id="inp_9_y" class="suma_y" value="0" value="0"></input>
	    		<input type="hidden" id="inp_plan_9"></input>
	    		<td><center><input id="inp_cuota_9" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_tc_10" class="tableHeadersRightSmaller form-control tc suma_2" type="text" placeholder="0"></input></center></td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_tarjeta_10" class="select2 s-width"></select>
			    	</center>
	    		</td>
	    		<td style="background-color: #999999 !important; border: 2px solid #000 !important;">
	    			<center>
		    			<select id="sel_cuota_10" class="select2 s-width"></center>
	    		</td>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cupon_10" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<input type="hidden" id="inp_cuota_10_coeficiente"></input>
	    		<input type="hidden" id="inp_plan_cuota_10"></input>
	    		<input type="hidden" id="inp_10_x" class="suma_x" value="0"></input>
	    		<input type="hidden" id="inp_10_y" class="suma_y" value="0"></input>
	    		<input type="hidden" id="inp_plan_10"></input>
	    		<td style="background-color: #999999 !important;"><center><input id="inp_cuota_10" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr style="border-top: 1px solid #000 !important;">
	    		<td class="white-font" rowspan="2" style="vertical-align: middle; background-color: #999999 !important; color: #000 !important; border-top: 2px solid #000 !important; border-bottom: 2px solid #000 !important;"><center>Presupuestar</center></td>
	    		<td class="white-font" rowspan="2" style="vertical-align: middle; background-color: #F7F7F8 !important; border-top: 2px solid #000 !important; border-bottom: 2px solid #000 !important;"><center><input id="inp_manual_presupuestar" class="tableHeadersRightSmaller form-control tc" type="text" placeholder="0"></input></center></td>
	    		<td class="white-font" rowspan="2" style="vertical-align: middle; background-color: #999999 !important; color: #000 !important; border: 2px solid #000 !important;" border-top: 2px solid #000 !important;><center>Ahora 12/18<br>Jue, Vie y Sab.</center></td>
	    		<td style="background-color: #F7F7F8 !important; border: solid 2px #000 !important; color: #000 !important;"><center><b>12</b></center></td>
	    		<td style="background-color: #C1C1C1 !important; border-top: 2px solid #000 !important;"><center><input id="inp_disponible_12" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<td style="background-color: #C1C1C1 !important; border-top: 2px solid #000 !important;"><center><input id="inp_utilizable_12" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>
	    	<tr style="border-bottom: 1px solid #000 !important;">
	    		<td style="background-color: #F7F7F8 !important; border: solid 2px #000 !important; color: #000 !important;"><center><b>18</b></center></td>
	    		<td style="background-color: #999999 !important; border-bottom: 2px solid #000 !important;"><center><input id="inp_disponible_18" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    		<td style="background-color: #999999 !important; border-bottom: 2px solid #000 !important;"><center><input id="inp_utilizable_18" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	    	</tr>

	    	<tr>
	    		<td class="white-font" style="vertical-align: middle; background-color: #DDDDDD !important;"><center>Observaci&oacute;n</center></td>
	    		<td colspan="5" style="vertical-align: middle; background-color: #DDDDDD !important;"><textarea id="inp_modelo_descripcion" cols="100%" rows="1"></textarea></td>
	    	</tr>

	    	<tr>
	    		<td class="white-font" style="vertical-align: middle; background-color: #000 !important; font-size: 18px !important; color: #FFF !important;"><center><b>Total</b></center></td>
	    		<td style="background-color: #000 !important;"><center><input id="inp_total_2" class="tableHeadersRightSmaller form-control" style="font-size: 18px !important; color: #FFF !important;" type="text" value="0"></input></center></td>
	    		<td style="background-color: #000 !important;"></td>
	    		<td style="background-color: #000 !important;"></td>
	    		<td style="vertical-align: middle; background-color: #000 !important; font-size: 18px !important; color: #FFF !important;"><b>DOLAR<b></td>
	    		<td style="background-color: #000 !important;"><input type="text" class="tableHeadersRightSmaller" id="dolar_ar" style="font-size: 18px !important; color: #FFF !important;" disabled="disabled"></input></td>
	    	</tr>
    	</tbody>
	</table>
</div>

<!-- -->

<div class="table-responsive col-xs-12 col-sm-12 col-md-2">
	<table class="table table-bordered">
		<thead>
			<colgroup span="1"></colgroup>
			<tr class="tableHead">
			    <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Resumen<br>Para cargar en Genver</b></center></th>
			</tr>
		</thead>

  		<tbody>
	  		<tr>
	  			<td style="vertical-align: middle; background-color: #F7F7F8 !important;" class="white-font"><center>Precio Propuesto</center></td>
	  			<td style="vertical-align: middle; background-color: #F7F7F8 !important;"><center><input id="inp_precio_propuesto" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	  		</tr>

	  		<tr>
	  			<td class="white-font"><center>Gesti&oacute;n</center></td>
	  			<td><center><input id="inp_gastos_gestion" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	  		</tr>

	  		<tr>
	  			<td style="vertical-align: middle; background-color: #F7F7F8 !important;" class="white-font"><center>Accesorios Cierre</center></td>
	  			<td style="vertical-align: middle; background-color: #F7F7F8 !important;"><center><input id="inp_accesorios_cierre" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	  		</tr>

	  		<tr>
	  			<td class="white-font"><center>Garant&iacute;a</center></td>
	  			<td><center><input id="inp_garantia" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	  		</tr>

	  		<tr>
	  			<td style="vertical-align: middle; background-color: #F7F7F8 !important;" class="white-font"><center>Gastos de Gestoria</center></td>
	  			<td style="vertical-align: middle; background-color: #F7F7F8 !important;"><center><input id="inp_gastos_gestoria" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	  		</tr>

	  		<tr>
	  			<td style="vertical-align: middle; background-color: #999999 !important; border-top: 1px solid #000 !important; border-bottom: 1px solid #000 !important;"><center><b>Total Final Genver</b></center></td>
	  			<td style="vertical-align: middle; background-color: #999999 !important; border-top: 1px solid #000 !important; border-bottom: 1px solid #000 !important;"><center><input id="inp_total_final_genver" class="tableHeadersRightSmallerBlack form-control" type="text" value="0"></input></center></td>
	  		</tr>

	  		<tr>
	  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>Retenci&oacute;n<br>AH 12/18 Todo P</center></td>
	  			<td style="vertical-align: middle; background-color: #F7F7F8 !important;"><center><input id="inp_retencion_12_18" class="tableHeadersRightSmaller form-control" type="text" value="0"></input></center></td>
	  		</tr>

	  		<tr class="tableHead">
			    <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Opciones</b></center></th>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('admin'); ?>" id="btn-extras" type="button" class="btn btn-default btn-block"><b>ADMINISTRACI&Oacute;N</b></a></center></td>
			</tr>
			
			<tr>
				<td colspan="2"><center><button id="b3" type="button" class="btn btn-default btn-block" data-toggle="modal" data-target=".bs-cuentas-plaza-motos-lg"><b>CUENTAS PLAZA MOTOS</b></button></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><button id="b4" type="button" class="btn btn-default btn-block" data-toggle="modal" data-target=".bs-numeros-utiles-lg"><b>NROS &Uacute;TILES</b></button></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><button id="btn_reiniciar" class="btn btn-danger btn-block"><b>REINICIAR</b></button></center></td>
			</tr>

			<tr>
				<td style="vertical-align: middle; background-color: #FFF !important;"><center><?=img($img_fondo_plazamotos) ?></center></td>
				<td style="vertical-align: middle; background-color: #FFF !important;"><center><?=img($img_2_plazamotos_10) ?></center></td>
			</tr>
  		</tbody>
  	</table>
</div>
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
<script>
	function consultarDolarARS(){
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: 'https://free.currencyconverterapi.com/api/v5/convert?q=USD_ARS&compact=y',
			success: function (data) {
				$('#dolar_ar').prop("value", data.USD_ARS.val);
			}
		});	
	}
	consultarDolarARS();	

	$(document).ready(function() {
		$.fn.select2.defaults.set('language', 'es');
	    $('.select2').select2({
	    	width: 'resolve',
	    	height: 'resolve',
	    });	  

	    $('#btn_reiniciar').on('click', function(e){
		    e.preventDefault();
		    $('input[type=text]').prop('value','0');
		    $('textarea').prop('value', '');
			$('.select2').val(null).trigger('change');
			$('#inp_categoria').prop('value','');
			$('.manual').prop('value','');
			$('input[type=hidden]').prop('value','0');
			$('#inp_presupuestar_12').prop('value','12');
			$('#inp_presupuestar_18').prop('value','18');
			$('.tc').prop('value','');
			$('.suma_x').prop('value','0');
			$('.suma_y').prop('value','0');
			consultarDolarARS();
		});

		$("#sel_operacion").select2({
		    ajax: {
			    url: "index.php/calculadora/c_calculadora/consultarOperaciones",
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
			      return {
			        q: params.term, 
			        page: params.page
			      };
			    },
			    processResults: function (data, params) {
			     	params.page = params.page || 1;

			      	return {
			        	results: data
			      	};
			    },
			    cache: true
			},
				placeholder: 'SELECCIONE...',
		});
			
	//--

		$("#sel_marca").select2({
			ajax: {
			    url: "index.php/calculadora/c_calculadora/consultarMarcas",
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
			      	return {
			        	q: params.term, 
			        	page: params.page
			      	};
			    },
			    processResults: function (data, params) {
			      	params.page = params.page || 1;

			      	return {
			        	results: data
			      	};
			    },
			    cache: true
			},
		  	placeholder: 'SELECCIONE...',
		});

	//--

		$('#sel_marca').on('change', function() {
	    	var marcaId = $('#sel_marca').val();  
	    	$("#sel_modelo").select2().empty();  	
			var modeloSelect = $('#sel_modelo');
				
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarModelos?marcaId=' + marcaId
			}).then(function (data) {
				var optionF = new Option('SELECCIONE...',null,true,true);
			   	modeloSelect.append(optionF).trigger('change');
			    
			   	for (var obj in data){
			   		var option = new Option(data[obj].text, data[obj].id, true, true);
			   		modeloSelect.append(option).trigger('change');	
			   	}	    
		    	$('#sel_modelo').select2().val("null").trigger("change");
			});
	    });

	//--

	    $('#sel_modelo, #dolar_ar').on('select2:select keyup', function() {
	    	var modeloId = $('#sel_modelo').val();  

	    	$('#inp_segun_modelo_operacion').prop("value", "0");
	    	$('#inp_categoria').prop("value", "");
	    	$('#inp_flete_operacion').prop("value", "0");
	    	$('#inp_accesorio_operacion').prop("value", "0");
	    	$('#inp_modelo_descripcion').prop("value", "");
	    	
	    	var dataO = $("#sel_operacion option:selected").text();    	
	    	var operacionId = dataO.toLowerCase();  
	    	var v_dolar = parseFloat($('#dolar_ar').val());
			
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarModelosPrecios?modeloId=' + modeloId + '&operacion=' + operacionId
			}).then(function (data) {
				for (var obj in data){
					var v_monto = data[obj].monto;
					if(v_monto === "0"){
						v_monto = parseFloat(data[obj].cdo * (v_dolar + 0.2) + data[obj].flete).toFixed(2);
						$('#inp_segun_modelo_operacion').prop("value", v_monto);	
						$('#inp_flete_operacion').prop("value", "");	
						$('#inp_flete_operacion').prop("disabled", "disabled");
					} else {
						$('#inp_segun_modelo_operacion').prop("value", v_monto);	
						$('#inp_flete_operacion').prop("disabled", "");
						$('#inp_flete_operacion').prop("value", data[obj].flete);	
					}		
					$('#inp_categoria').prop("value", data[obj].categoria);						
					$('#inp_accesorio_operacion').prop("value", data[obj].accesorio);
					$('#inp_modelo_descripcion').prop("value", data[obj].descripcion);
				}
				$('.suma_1').trigger("blur");		    
			});   

			$('#sel_patentamiento').val(null).trigger("change");	
			$('#inp_patentamiento').prop("value", 0);	

			$('#sel_garantias').val(null).trigger("change");	
			$('#inp_sellado').prop("value", 0);	
	    });

	//--

		$('#sel_operacion').on('change', function() {
	    	var operacionId = $('#sel_operacion').val();  
	    	$("#sel_gestion").select2().empty();
			var gestionSelect = $('#sel_gestion');
				
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarGestiones?operacionId=' + operacionId
			}).then(function (data) {
				var optionF = new Option('SELECCIONE...',null,true,true);
			    gestionSelect.append(optionF).trigger('change');
			    
			    for (var obj in data){
			    	var option = new Option(data[obj].text, data[obj].id, true, true);
			    	gestionSelect.append(option).trigger('change');	
			    }	    

			    $('#sel_gestion').select2().val("null").trigger("change");
			    $('#inp_gestion').prop("value", 0);
			});

			var modeloId = $('#sel_modelo').val();
			if(modeloId != null){
				var modeloId = $('#sel_modelo').val();  

		    	$('#inp_segun_modelo_operacion').prop("value", "0");
		    	$('#inp_categoria').prop("value", "");
		    	$('#inp_flete_operacion').prop("value", "0");
		    	$('#inp_accesorio_operacion').prop("value", "0");
		    	$('#inp_modelo_descripcion').prop("value", "");
		    	
		    	var dataO = $("#sel_operacion option:selected").text();    	
		    	var operacionId = dataO.toLowerCase();  
				
				$.ajax({
				    type: 'GET',
					url: 'index.php/calculadora/c_calculadora/consultarModelosPrecios?modeloId=' + modeloId + '&operacion=' + operacionId
				}).then(function (data) {
				    for (var obj in data){
						for (var obj in data){
							var v_monto = data[obj].monto;
							if(v_monto === "0"){
								v_monto = parseFloat(data[obj].cdo * (v_dolar + 0.2) + data[obj].flete).toFixed(2);
								$('#inp_segun_modelo_operacion').prop("value", v_monto);	
								$('#inp_flete_operacion').prop("value", "");	
								$('#inp_flete_operacion').prop("disabled", "disabled");
							} else {
								$('#inp_segun_modelo_operacion').prop("value", v_monto);	
								$('#inp_flete_operacion').prop("disabled", "");
								$('#inp_flete_operacion').prop("value", data[obj].flete);	
							}				
							$('#inp_categoria').prop("value", data[obj].categoria);						
							$('#inp_accesorio_operacion').prop("value", data[obj].accesorio);
							$('#inp_modelo_descripcion').prop("value", data[obj].descripcion);
						}
					}		
					$('.suma_1').trigger("blur");    
				}); 
			}
			$('#sel_patentamiento').val(null).trigger("change");	
			$('#inp_patentamiento').prop("value", 0);	

			$('#sel_garantias').val(null).trigger("change");	
			$('#inp_sellado').prop("value", 0);	
	    })	

	//--

	    $('#sel_gestion').on('select2:select', function() {
			var gestionId = $('#sel_gestion').val();  
	    	$('#inp_gestion').prop("value", "0");
			
			$.ajax({
			    type: 'GET',
			    url: 'index.php/calculadora/c_calculadora/consultarGestionesPrecios?gestionId=' + gestionId
			}).then(function (data) {
			    for (var obj in data){
					$('#inp_gestion').prop("value", data[obj].monto);	
				}		    
				$('.suma_1').trigger("blur");
			}); 
	    });
			
	//--

		$("#sel_patentamiento").select2({
	    	ajax: {
			    url: "index.php/calculadora/c_calculadora/consultarPatentamientos",
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
				    return {
			        	q: params.term, 
			        	page: params.page
			      	};
			    },
			    processResults: function (data, params) {
			      	params.page = params.page || 1;

			      	return {
			        	results: data
			      	};
			    },
			    cache: true
		    },
		    placeholder: 'SELECCIONE...',
		});

		$('#sel_patentamiento').on('select2:select', function() {
			var patentamientoId = $('#sel_patentamiento').val();  
	    	$('#inp_patentamiento').prop("value", "0");
	    	var montoOperacion = $("#inp_segun_modelo_operacion").val();
			
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarPatentamientosPrecios?patentamientoId=' + patentamientoId
			}).then(function (data) {
			    for (var obj in data){
			    	$('#inp_patentamiento').prop("value", Math.round( parseFloat(data[obj].monto) + (montoOperacion*(data[obj].factor/100)) ));		
				}		    
				$('.suma_1').trigger("blur");
			}); 
	    });

	//--

		$("#sel_accesorios").select2({
	   	    ajax: {
			    url: "index.php/calculadora/c_calculadora/consultarAccesorios",
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
			      	return {
			        	q: params.term, 
			        	page: params.page
			      	};
			    },
			    processResults: function (data, params) {
			      	params.page = params.page || 1;

			      	return {
			        	results: data
			      	};
			    },
			    cache: true
			},
		    placeholder: 'SELECCIONE...',
		});

		$('#sel_accesorios').on('select2:select', function() {
			var accesorioId = $('#sel_accesorios').val();  
	    	$('#inp_accesorios').prop("value", "0");
			
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarAccesoriosPrecios?accesorioId=' + accesorioId
			}).then(function (data) {
			    for (var obj in data){
						$('#inp_accesorios').prop("value", data[obj].monto);	
				}		    
				$('.suma_1').trigger("blur");
			}); 
	    });

	//--

		$("#sel_garantias").select2({
			ajax: {
			    url: "index.php/calculadora/c_calculadora/consultarGarantias",
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
			    	return {
			        	q: params.term, 
			        	page: params.page
			      	};
			    },
			    processResults: function (data, params) {
			      	params.page = params.page || 1;

			      	return {
			        	results: data
			      	};
			    },
			    cache: true
			},
			placeholder: 'SELECCIONE...',  
		});

		$('#sel_garantias').on('select2:select', function() {
			var garantiaId = $('#sel_garantias').val();  
	    	$('#inp_sellado').prop("value", "0");
	    	var montoOperacion = $("#inp_segun_modelo_operacion").val();
			
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarGarantiasPrecios?garantiaId=' + garantiaId
			}).then(function (data) {
				for (var obj in data){
					$('#inp_sellado').prop("value", Math.round(montoOperacion * data[obj].factor));	
				}		    
				$('.suma_1').trigger("blur");
			}); 
	  });

	//--

		$("#sel_extras").select2({
   	    ajax: {
		    url: "index.php/calculadora/c_calculadora/consultarExtras",
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      	return {
		        	q: params.term, 
		        	page: params.page
		      	};
		    },
		    processResults: function (data, params) {
		      	params.page = params.page || 1;

		      	return {
		        	results: data
		      	};
		    },
		    cache: true
			},
			placeholder: 'SELECCIONE...',
		});


		$('#sel_extras').on('select2:select', function() {
			var extraId = $('#sel_extras').val();  
	    	$('#inp_extra').prop("value", "0");
			
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarExtrasPrecios?extraId=' + extraId
			}).then(function (data) {
			    for (var obj in data){
					$('#inp_extra').prop("value", data[obj].monto);	
				}		    
				$('.suma_1').trigger("blur");
			}); 
	    });

	//--

		$('.suma_1, .suma_2, .suma_x, .suma_y').on("blur keyup change", function () {
	    var total_1 = 0;
	    $('.suma_1').each(function() {
	      total_1 += Number($(this).val());
	    });

	 		$('#inp_total_1').prop("value", total_1);

	 		var total_2 = 0;
	 		$('.suma_2').each(function() {
		    total_2 += Number($(this).val());
		  });

	 		var t2 = (total_1 - total_2);
	    $('#inp_total_2').prop("value", t2);

	    var valor_precio_propuesto = parseFloat( ($('#inp_segun_modelo_operacion').val() !== "") ? $('#inp_segun_modelo_operacion').val() : "0" );
	    $('#inp_precio_propuesto').prop("value", valor_precio_propuesto);

	    var total_x = 0;
	    $('.suma_x').each(function() {
	      total_x += Number($(this).val());
	    });

	    var total_y = 0;
	    $('.suma_y').each(function() {
	      total_y += Number($(this).val());
	    });

	    var total_z = 0;
	    total_z = Math.round(total_x - total_y);
	    $('#inp_retencion_12_18').prop("value", total_z);

	    var valor_gastos_gestion = 0;
	    var v_gestion = parseFloat( ($('#inp_gestion').val() !== "") ? $('#inp_gestion').val() : "0" );
	    var v_retencion_crediticia = parseFloat( ($('#inp_retencion_crediticia').val() !== "") ? $('#inp_retencion_crediticia').val() : "0" );
	    var v_flete_operacion = parseFloat( ($('#inp_flete_operacion').val() !== "") ? $('#inp_flete_operacion').val() : "0" );
	    var v_flete_verificacion = parseFloat( ($('#inp_flete_verificacion').val() !== "") ? $('#inp_flete_verificacion').val() : "0" );
	    var v_retencion_12_18 = parseFloat( ($('#inp_retencion_12_18').val() !== "") ? $('#inp_retencion_12_18').val() : "0" );
	    valor_gastos_gestion = Math.round(v_gestion + v_retencion_crediticia + v_flete_operacion + v_flete_verificacion + v_retencion_12_18);
	    $('#inp_gastos_gestion').prop("value", valor_gastos_gestion);

	    var valor_accesorios_cierre = 0;
	    var v_accesorio = parseFloat( ($('#inp_accesorios').val() !== "") ? $('#inp_accesorios').val() : "0" );
	    var v_accesorio_operacion = parseFloat( ($('#inp_accesorio_operacion').val() !== "") ? $('#inp_accesorio_operacion').val() : "0" );
	    var v_accesorio_extra_manual = parseFloat( ($('#inp_accesorio_extra_manual').val() !== "") ? $('#inp_accesorio_extra_manual').val() : "0" );
	    valor_accesorios_cierre = Math.round(v_accesorio + v_accesorio_operacion + v_accesorio_extra_manual);
	    $('#inp_accesorios_cierre').prop("value", valor_accesorios_cierre);

	    var valor_garantia = 0;
	    var v_garantia = parseFloat( ($('#inp_sellado').val() !== "") ? $('#inp_sellado').val() : "0" );
	    var v_sellado = parseFloat( ($('#inp_sellado_2').val() !== "") ? $('#inp_sellado_2').val() : "0" );
	    valor_garantia = Math.round(v_garantia + v_sellado);
	    $('#inp_garantia').prop("value", valor_garantia);

	    var valor_gastos_gestoria = 0;
	    var v_patentamiento = parseFloat( ($('#inp_patentamiento').val() !== "") ? $('#inp_patentamiento').val() : "0" );
	    var v_extra = parseFloat( ($('#inp_extra').val() !== "") ? $('#inp_extra').val() : "0" );
	    valor_gastos_gestoria = Math.round(v_patentamiento + v_extra);
	    $('#inp_gastos_gestoria').prop("value", valor_gastos_gestoria);

	    var valor_total_final_genver = 0;
	    valor_total_final_genver = (valor_precio_propuesto + valor_gastos_gestion + valor_accesorios_cierre + valor_garantia + valor_gastos_gestoria);
	    $('#inp_total_final_genver').prop("value", valor_total_final_genver);
		});		

	//--

		$("#sel_medio_pago").select2({
			ajax: {
			    url: "index.php/calculadora/c_calculadora/consultarSolicitados",
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
				    return {
			    	    q: params.term, 
			        	page: params.page
			      	};
			    },
			    processResults: function (data, params) {
			      	params.page = params.page || 1;

			      	return {
			        	results: data
			      	};
			    },
			    cache: true
			},
			placeholder: 'SELECCIONE...',
		});

		function fun_solicitado() {
			$('#inp_credito_s').prop("value", "0");
			var manual_solicitado = $('#inp_manual_s').val();
			var valor_solicitado = $('#inp_solicitado_s').val();
			var credito_solicitado = Math.round(manual_solicitado * valor_solicitado);
			$('#inp_credito_s').prop("value", credito_solicitado);		
				
			$('#inp_retencion_s').prop("value", "0");
			var valor_solicitado = $('#inp_solicitado_s').val();
			var credito_solicitado = $('#inp_credito_s').val();
			var retencion_solicitado = Math.round(credito_solicitado - (credito_solicitado / valor_solicitado));
			$('#inp_retencion_s').prop("value", retencion_solicitado);
			$('#inp_retencion_crediticia').prop("value", retencion_solicitado);	
		}

		function fun_otorgado() {
			$('#inp_credito_o').prop("value", "0");
			var manual_otorgado = $('#inp_manual_o').val();
			var valor_otorgado = $('#inp_otorgado_o').val();
			var credito_otorgado = (manual_otorgado / valor_otorgado).toFixed(4);
			$('#inp_credito_o').prop("value", credito_otorgado).trigger("blur");				

			$('#inp_retencion_o').prop("value", "0");
			var manual_otorgado = $('#inp_manual_o').val();
			var otorgado_o = $('#inp_otorgado_o').val();
			var retencion_otorgado = Math.round(manual_otorgado - (manual_otorgado / otorgado_o));
			$('#inp_retencion_o').prop("value", retencion_otorgado); 
			$('#inp_retencion_crediticia').prop("value", retencion_otorgado).trigger("blur");
		}

		$('#sel_medio_pago').on('select2:select', function() {
			var solicitadoId = $('#sel_medio_pago').val();  
	    	$('#inp_solicitado_s').prop("value", "0");
	    	$('#inp_otorgado_o').prop("value", "0");
			
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarSolicitadosPrecios?solicitadoId=' + solicitadoId
			}).then(function (data) {
			    for (var obj in data){
					$('#inp_solicitado_s').prop("value", data[obj].monto);	
					$('#inp_otorgado_o').prop("value", data[obj].monto);	
				}				   
				fun_solicitado();
				fun_otorgado();				
			}); 
	    });

	    $('#inp_manual_s').on('change keyup', function() {
	    	fun_solicitado();
	    });

	    $('#inp_manual_o').on('change keyup', function(){
	    	fun_otorgado();
	    });

	//--

		$("#sel_tarjeta_1,#sel_tarjeta_2,#sel_tarjeta_3,#sel_tarjeta_4,#sel_tarjeta_5,#sel_tarjeta_6,#sel_tarjeta_7,#sel_tarjeta_8,#sel_tarjeta_9,#sel_tarjeta_10").select2({
		  	ajax: {
			    url: "index.php/calculadora/c_calculadora/consultarTarjetas",
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
			      	return {
			        	q: params.term, 
			        	page: params.page
			      	};
			    },
			    processResults: function (data, params) {
			      	params.page = params.page || 1;

			      	return {
			        	results: data
			      	};
			    },
			    cache: true
			},
				placeholder: 'SELECCIONE...',
		});

	//--

		$("#sel_tarjeta_1").on('change', function() {
			var tarjeta1 = $('#sel_tarjeta_1').val();  
	    	$("#sel_cuota_1").select2().empty(); 	
			var cuotaSelect = $('#sel_cuota_1');
				
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarCuotas?tarjeta=' + tarjeta1
			}).then(function (data) {
				var optionC = new Option('SELECCIONE...',null,true,true);
				cuotaSelect.append(optionC).trigger('change');
			    
			    for (var obj in data){
			    	var option = new Option(data[obj].text, data[obj].id, true, true);
			    	cuotaSelect.append(option).trigger('change');	
			    }	    

			    $('#sel_cuota_1').select2().val("null").trigger("change");
			});
		});

		function fun_calcularUtilizable(){
			var val_disponible = $('#inp_manual_disponible').val();
			var val_coeficiente = $('#inp_cuota_1_coeficiente').val();
			var utilizable = Math.round(val_disponible / val_coeficiente);
			$('#inp_utilizable').prop("value", utilizable);
		}

		$("#sel_cuota_1").on('select2:select', function() {
			var cuota1 = $('#sel_cuota_1').val();  
	    	$('#inp_cuota_1_coeficiente').prop("value", "0");
			
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarCuotasCoeficientes?cuota=' + cuota1
			}).then(function (data) {
			    for (var obj in data){
					$('#inp_cuota_1_coeficiente').prop("value", data[obj].coeficiente);	
				}		    
				fun_calcularUtilizable()
			}); 
		});

		$('#inp_manual_disponible').on('keyup', function() {
			fun_calcularUtilizable();
		});

	//--

		function fun_cargarCuotas(sel_tarjeta,sel_cuota){
			var tarjeta = $(sel_tarjeta).val();  
	    	$(sel_cuota).select2().empty();
	    	
			var cuotaSelect = $(sel_cuota);
				
			$.ajax({
			    type: 'GET',
				url: 'index.php/calculadora/c_calculadora/consultarCuotas?tarjeta=' + tarjeta
			}).then(function (data) {
				var optionC = new Option('SELECCIONE...',null,true,true);
			    cuotaSelect.append(optionC).trigger('change');
			    
			    for (var obj in data){
			    	var option = new Option(data[obj].text, data[obj].id, true, true);
			    	cuotaSelect.append(option).trigger('change');	
			    }	    

			    $(sel_cuota).select2().val("null").trigger("change");
			});
		}

		$("#sel_tarjeta_2").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_2','#sel_cuota_2');
		});

		$("#sel_tarjeta_3").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_3','#sel_cuota_3');
		});

		$("#sel_tarjeta_4").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_4','#sel_cuota_4');
		});

		$("#sel_tarjeta_5").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_5','#sel_cuota_5');
		});

		$("#sel_tarjeta_6").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_6','#sel_cuota_6');
		});

		$("#sel_tarjeta_7").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_7','#sel_cuota_7');
		});

		$("#sel_tarjeta_8").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_8','#sel_cuota_8');
		});

		$("#sel_tarjeta_9").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_9','#sel_cuota_9');
		});

		$("#sel_tarjeta_10").on('change', function() {
			fun_cargarCuotas('#sel_tarjeta_10','#sel_cuota_10');
		});

	//--

		function fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon){
			var val_tc = tarjeta_credito;
			var val_ct = coeficiente_tarjeta;
			var cupon = Math.round(val_tc * val_ct);
			$(nombre_cupon).prop("value","0");
			$(nombre_cupon).prop("value",cupon);
		}

		function fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota){
			var val_cupon = valor_cupon;
			var val_plan_cuota = plan_cuota;
			var cuota = "0";
			if((val_cupon !== 0 && valor_cupon !== null && valor_cupon !== "") && (val_plan_cuota !== 0 && val_plan_cuota !== null && val_plan_cuota !== "")){
				cuota = Math.round(valor_cupon / val_plan_cuota);
			}			
			$(inp_cuota).prop("value","0");
			$(inp_cuota).prop("value",cuota);
		}

		function fun_calculo_TARJETAS_CREDITO(sel_cuota,inp_cuota_coeficiente,inp_plan_cuota,inp_tc,inp_cupon,inp_cuota,inp_x,inp_y,inp_plan) {
			$(sel_cuota).on('select2:select', function() {
				var cuota = $(sel_cuota).val();  
			   	$(inp_cuota_coeficiente).prop("value", "0");
				var valor_cupon = "";
			
				$.ajax({
				    type: 'GET',
					url: 'index.php/calculadora/c_calculadora/consultarCuotasCoeficientes?cuota=' + cuota
				}).then(function (data) {
				    for (var obj in data){
						$(inp_cuota_coeficiente).prop("value", data[obj].coeficiente);
						$(inp_plan_cuota).prop("value", data[obj].cuota);	
						$(inp_plan).prop("value", data[obj].plan);
					}		    
					var tarjeta_credito = $(inp_tc).val();
					var coeficiente_tarjeta = $(inp_cuota_coeficiente).val();
					var nombre_cupon = inp_cupon;
					fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);										
				}).then(function () {
					valor_cupon = $(inp_cupon).val();
					var plan_cuota = $(inp_plan_cuota).val();
					fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);
				}).then(function () {
					var plan = $(inp_plan).val();
					if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
						$(inp_x).prop("value", valor_cupon);
						var inp_tc_value = $(inp_tc).val();
						$(inp_y).prop("value", inp_tc_value);
					} else {
						$(inp_x).prop("value", 0);
						$(inp_y).prop("value", 0);
					}
					$('.suma_1, .suma_2, .suma_x, .suma_y').trigger('blur');
				}); 
			});
		}

	//--

		$("#sel_cuota_2").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_2','#inp_cuota_2_coeficiente','#inp_plan_cuota_2','#inp_tc_2','#inp_cupon_2','#inp_cuota_2','#inp_2_x','#inp_2_y','#inp_plan_2');
		});

		$('#inp_tc_2').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_2').val();
			var coeficiente_tarjeta = $('#inp_cuota_2_coeficiente').val();
			var nombre_cupon = '#inp_cupon_2';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_2').val();
			var plan_cuota = $('#inp_plan_cuota_2').val();
			var inp_cuota = '#inp_cuota_2';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_2').val();
			var inp_x = "#inp_2_x";
			var inp_y = "#inp_2_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				$(inp_y).prop("value", $('#inp_tc_2').val());
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--		

		$("#sel_cuota_3").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_3','#inp_cuota_3_coeficiente','#inp_plan_cuota_3','#inp_tc_3','#inp_cupon_3','#inp_cuota_3','#inp_3_x','#inp_3_y','#inp_plan_3');
		});

		$('#inp_tc_3').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_3').val();
			var coeficiente_tarjeta = $('#inp_cuota_3_coeficiente').val();
			var nombre_cupon = '#inp_cupon_3';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_3').val();
			var plan_cuota = $('#inp_plan_cuota_3').val();
			var inp_cuota = '#inp_cuota_3';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_3').val();
			var inp_x = "#inp_3_x";
			var inp_y = "#inp_3_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_3').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--

		$("#sel_cuota_4").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_4','#inp_cuota_4_coeficiente','#inp_plan_cuota_4','#inp_tc_4','#inp_cupon_4','#inp_cuota_4','#inp_4_x','#inp_4_y','#inp_plan_4');
		});

		$('#inp_tc_4').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_4').val();
			var coeficiente_tarjeta = $('#inp_cuota_4_coeficiente').val();
			var nombre_cupon = '#inp_cupon_4';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_4').val();
			var plan_cuota = $('#inp_plan_cuota_4').val();
			var inp_cuota = '#inp_cuota_4';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_4').val();
			var inp_x = "#inp_4_x";
			var inp_y = "#inp_4_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_4').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--

		$("#sel_cuota_5").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_5','#inp_cuota_5_coeficiente','#inp_plan_cuota_5','#inp_tc_5','#inp_cupon_5','#inp_cuota_5','#inp_5_x','#inp_5_y','#inp_plan_5');
		});

		$('#inp_tc_5').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_5').val();
			var coeficiente_tarjeta = $('#inp_cuota_5_coeficiente').val();
			var nombre_cupon = '#inp_cupon_5';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_5').val();
			var plan_cuota = $('#inp_plan_cuota_5').val();
			var inp_cuota = '#inp_cuota_5';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_5').val();
			var inp_x = "#inp_5_x";
			var inp_y = "#inp_5_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_5').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--

		$("#sel_cuota_6").on('select2:select chamge', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_6','#inp_cuota_6_coeficiente','#inp_plan_cuota_6','#inp_tc_6','#inp_cupon_6','#inp_cuota_6','#inp_6_x','#inp_6_y','#inp_plan_6');
		});

		$('#inp_tc_6').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_6').val();
			var coeficiente_tarjeta = $('#inp_cuota_6_coeficiente').val();
			var nombre_cupon = '#inp_cupon_6';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_6').val();
			var plan_cuota = $('#inp_plan_cuota_6').val();
			var inp_cuota = '#inp_cuota_6';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_6').val();
			var inp_x = "#inp_6_x";
			var inp_y = "#inp_6_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_6').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--

		$("#sel_cuota_7").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_7','#inp_cuota_7_coeficiente','#inp_plan_cuota_7','#inp_tc_7','#inp_cupon_7','#inp_cuota_7','#inp_7_x','#inp_7_y','#inp_plan_7');
		});

		$('#inp_tc_7').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_7').val();
			var coeficiente_tarjeta = $('#inp_cuota_7_coeficiente').val();
			var nombre_cupon = '#inp_cupon_7';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_7').val();
			var plan_cuota = $('#inp_plan_cuota_7').val();
			var inp_cuota = '#inp_cuota_7';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_7').val();
			var inp_x = "#inp_7_x";
			var inp_y = "#inp_7_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_7').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--

		$("#sel_cuota_8").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_8','#inp_cuota_8_coeficiente','#inp_plan_cuota_8','#inp_tc_8','#inp_cupon_8','#inp_cuota_8','#inp_8_x','#inp_8_y','#inp_plan_8');
		});

		$('#inp_tc_8').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_8').val();
			var coeficiente_tarjeta = $('#inp_cuota_8_coeficiente').val();
			var nombre_cupon = '#inp_cupon_8';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_8').val();
			var plan_cuota = $('#inp_plan_cuota_8').val();
			var inp_cuota = '#inp_cuota_8';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_8').val();
			var inp_x = "#inp_8_x";
			var inp_y = "#inp_8_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_8').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--

		$("#sel_cuota_9").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_9','#inp_cuota_9_coeficiente','#inp_plan_cuota_9','#inp_tc_9','#inp_cupon_9','#inp_cuota_9','#inp_9_x','#inp_9_y','#inp_plan_9');
		});

		$('#inp_tc_9').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_9').val();
			var coeficiente_tarjeta = $('#inp_cuota_9_coeficiente').val();
			var nombre_cupon = '#inp_cupon_9';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_9').val();
			var plan_cuota = $('#inp_plan_cuota_9').val();
			var inp_cuota = '#inp_cuota_9';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_9').val();
			var inp_x = "#inp_9_x";
			var inp_y = "#inp_9_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_9').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

		//--

		$("#sel_cuota_10").on('select2:select change', function() {
			fun_calculo_TARJETAS_CREDITO('#sel_cuota_10','#inp_cuota_10_coeficiente','#inp_plan_cuota_10','#inp_tc_10','#inp_cupon_10','#inp_cuota_10','#inp_10_x','#inp_10_y','#inp_plan_10');
		});

		$('#inp_tc_10').on('keyup change', function() {
			var tarjeta_credito = $('#inp_tc_10').val();
			var coeficiente_tarjeta = $('#inp_cuota_10_coeficiente').val();
			var nombre_cupon = '#inp_cupon_10';
			fun_calcularCupon(tarjeta_credito,coeficiente_tarjeta,nombre_cupon);

			var valor_cupon = $('#inp_cupon_10').val();
			var plan_cuota = $('#inp_plan_cuota_10').val();
			var inp_cuota = '#inp_cuota_10';
			fun_calcularCuota(valor_cupon,plan_cuota,inp_cuota);

			var plan = $('#inp_plan_10').val();
			var inp_x = "#inp_10_x";
			var inp_y = "#inp_10_y";
			if((plan === 'AH 12') || (plan === 'AH 18') || (plan === 'TODO P')) {
				$(inp_x).prop("value", $(nombre_cupon).val());
				var inp_tc_value = $('#inp_tc_10').val();
				$(inp_y).prop("value", inp_tc_value);
			} else {
				$(inp_x).prop("value", 0);
				$(inp_y).prop("value", 0);
			}
		});

	//--

		$('#inp_manual_presupuestar').on('keyup change', function() {
			var coeficiente_12 = 1.23;
			var coeficiente_18 = 1.28;
			var presupuestar = $('#inp_manual_presupuestar').val();

			var disponible_12 = Math.round(presupuestar * coeficiente_12);
			$('#inp_disponible_12').prop("value",disponible_12);

			var presupuestar_12 = 12;
			var utilizable_12 = Math.round(disponible_12 / presupuestar_12);
			$('#inp_utilizable_12').prop("value",utilizable_12);

			var disponible_18 = Math.round(presupuestar * coeficiente_18);
			$('#inp_disponible_18').prop("value",disponible_18);

			var presupuestar_18 = 18;
			var utilizable_18 = Math.round(disponible_18 / presupuestar_18);
			$('#inp_utilizable_18').prop("value",utilizable_18);
				
		});

	});//onReady
</script>