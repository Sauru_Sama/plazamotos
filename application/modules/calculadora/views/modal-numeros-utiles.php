<div class="modal fade bs-numeros-utiles-lg" tabindex="-1" role="dialog" aria-labelledby="numerosUtiles" >
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content" style="background-color: #F7F7F8;">
    	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">N&uacute;meros &Uacute;tiles</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
	      	<div class="table-responsive col-xs-12 col-sm-12 col-md-6">
						<table class="table table-bordered">
							<thead>
								<colgroup span="1"></colgroup>
								<tr class="tableHead">
								  <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Consultas de Saldo</b></center></th>
								</tr>
								<tr>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>Tarjeta</b></center></th>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>N&uacute;mero</b></center></th>
								</tr>
							</thead>

					  	<tbody>
					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>VISA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>011 4379-3400</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>MASTERCARD</center></td>
					  			<td class="white-font"><center>011 4348-7000</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>AMERICAN EXPRESS</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0800 444 2450</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>NARANJA</center></td>
					  			<td class="white-font"><center>0810 333 6272</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>CORDOBESA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0810 122 4445</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>CABAL</center></td>
					  			<td class="white-font"><center>0810 888 4500</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>SHOPPING</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0810 333 6666</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>GRUPAR</center></td>
					  			<td class="white-font"><center>0810 888 4787</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>NATIVA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0810 333 6284</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>SOL</center></td>
					  			<td class="white-font"><center>0351 429 8953</center></td>
					  		</tr>
					  	</tbody>
					  </table>
					</div>

				<!---->

					<div class="table-responsive col-xs-12 col-sm-12 col-md-6">
						<table class="table table-bordered">
							<thead>
								<colgroup span="1"></colgroup>
								<tr class="tableHead">
								  <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Autorizaciones</b></center></th>
								</tr>
								<tr>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>Tarjeta</b></center></th>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>N&uacute;mero</b></center></th>
								</tr>
							</thead>

					  	<tbody>
					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>VISA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>011 4378-4440</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>MASTERCARD</center></td>
					  			<td class="white-font"><center>011 4340-9800</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>AMERICAN EXPRESS</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0810 555 8000</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>NARANJA</center></td>
					  			<td class="white-font"><center>0810 555 6272</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>CORDOBESA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>011 4340-5600</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>CABAL</center></td>
					  			<td class="white-font"><center>0810 345 2528</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>SHOPPING</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0810 222 9333</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>GRUPAR</center></td>
					  			<td class="white-font"><center>0810 555 4787</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>NATIVA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0810 333 6284</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>SOL</center></td>
					  			<td class="white-font"><center>0351 429-8653</center></td>
					  		</tr>
					  	</tbody>
					  </table>
					</div>
				</div>

				<!---->

				<div class="row">
					<div class="table-responsive col-xs-12 col-sm-12 col-md-12">
						<table class="table table-bordered">
							<thead>
								<colgroup span="1"></colgroup>
								<tr class="tableHead">
								  <th colspan="4" scope="colgroup" class="cabecera-tabla"><center><b>Locales</b></center></th>
								</tr>
								<tr>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>Local</b></center></th>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>N&uacute;mero</b></center></th>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>Provincia</b></center></th>
									<th colspan="1" scope="colgroup" style="background-color: #dddddd;"><center><b>Direcci&oacute;n</b></center></th>
								</tr>
							</thead>

					  	<tbody>
					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>MAIPU - CASA CENTRAL</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0351 - 4219327</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>CORDOBA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>MAIPU 410</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>RIVERITA</center></td>
					  			<td class="white-font"><center>0351 - 4233197</center></td>
					  			<td class="white-font"><center>CORDOBA</center></td>
					  			<td class="white-font"><center>R. INDARTE 520</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>RIVERA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0351 - 4283974</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>CORDOBA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>R. INDARTE 563</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>ROSARIO (PLAZA)</center></td>
					  			<td class="white-font"><center>0351 - 4227944</center></td>
					  			<td class="white-font"><center>CORDOBA</center></td>
					  			<td class="white-font"><center>ROSARIO STA FE 67</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>LIBERTAD - MULTI</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0351 - 4280827</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>CORDOBA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>LIBERTAD 150</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>PREMIUM</center></td>
					  			<td class="white-font"><center>0351 - 4464004</center></td>
					  			<td class="white-font"><center>CORDOBA</center></td>
					  			<td class="white-font"><center>SAGRADA FAMILIA 400</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>SAN MARTIN</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0351 - 4256915</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>CORDOBA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>SAN MARTIN 498</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>PELAGIO LUNA GRANDE</center></td>
					  			<td class="white-font"><center>0380 - 4461991</center></td>
					  			<td class="white-font"><center>LA RIOJA</center></td>
					  			<td class="white-font"><center>PELAGIO B. LUNA 529</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>PELAGIO LUNA CHICO</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0380 - 4465100</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>LA RIOJA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>PELAGIO B. LUNA 744</center></td>
					  		</tr>

					  		<tr>
					  			<td class="white-font"><center>BAZAN</center></td>
					  			<td class="white-font"><center>0380 - 4461991</center></td>
					  			<td class="white-font"><center>LA RIOJA</center></td>
					  			<td class="white-font"><center>BAZAN Y BUSTOS 202</center></td>
					  		</tr>			

					  		<tr>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>JUNIN</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>0383 - 4427322</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>CATAMARCA</center></td>
					  			<td class="white-font" style="background-color: #F7F7F8 !important;"><center>JUNIN 1016</center></td>
					  		</tr>	  		  		
					  	</tbody>
					  </table>
					</div>
				</div>
      </div>
    </div>
  </div>
</div>