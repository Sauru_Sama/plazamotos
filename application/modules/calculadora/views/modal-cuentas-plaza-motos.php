<div class="modal fade bs-cuentas-plaza-motos-lg" tabindex="-1" role="dialog" aria-labelledby="cuentasPlazaMotos">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Cuentas Plaza Motos</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="table-responsive col-xs-12 col-sm-12 col-md-3">
            <table class="table table-bordered">
              <thead>
                <colgroup span="1"></colgroup>
                <tr class="tableHead">
                  <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Banco Santander</b></center></th>
                </tr> 
              </thead>

              <tbody>
                <tr>
                  <td style="background-color: #F7F7F8 !important;">
                    <center><img class="img-responsive" src="<?= base_url("assets/imagenes/banco-santander.png"); ?>" alt="Banco Santander"></center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font"">
                    <center>
                      SUC: 247 – CAPITALINAS       
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>
                      Cta. Cte. $ Nº 247-000042338           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font"">
                    <center>          
                      CBU: 0720247820000000423386
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>      
                      Titular: PLAZA MOTOS S.A.           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>         
                      CUIT: 30-71023848-7
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <!---->
          
          <div class="table-responsive col-xs-12 col-sm-12 col-md-3">
            <table class="table table-bordered">
              <thead>
                <colgroup span="1"></colgroup>
                <tr class="tableHead">
                  <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Banco Patagonia</b></center></th>
                </tr> 
              </thead>

              <tbody>
                <tr>
                  <td style="background-color: #F7F7F8 !important;">
                    <center><img class="img-responsive" src="<?= base_url("assets/imagenes/banco-patagonia.png"); ?>" alt="Banco Patagonia"></center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>
                      SUC: 070 - CÓRDOBA            
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>
                      Cta. Cte. $ Nº 709834365            
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>          
                      CBU: 0340070800709834365016           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>      
                      Titular: PLAZA MOTOS S.A.           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>         
                      CUIT: 30-71023848-7                                     
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <!---->

          <div class="table-responsive col-xs-12 col-sm-12 col-md-3">
            <table class="table table-bordered">
              <thead>
                <colgroup span="1"></colgroup>
                <tr class="tableHead">
                  <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Banco Galicia</b></center></th>
                </tr> 
              </thead>

              <tbody>
                <tr>
                  <td style="background-color: #F7F7F8 !important;">
                    <center><img class="img-responsive" src="<?= base_url("assets/imagenes/banco-galicia.png"); ?>" alt="Banco Galicia"></center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>
                      SUC: 138 - CATEDRAL           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>
                      Cta. Cte. $ Nº 8269-1 138-3                               
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>          
                      CBU: 0070138520000008269133                               
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>      
                      Titular: PLAZA MOTOS S.A.           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>         
                      CUIT: 30-71023848-7                      
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <!---->

          <div class="table-responsive col-xs-12 col-sm-12 col-md-3">
            <table class="table table-bordered">
              <thead>
                <colgroup span="1"></colgroup>
                <tr class="tableHead">
                  <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Banco de Cordoba</b></center></th>
                </tr> 
              </thead>

              <tbody>
                <tr>
                  <td style="background-color: #F7F7F8 !important;">
                    <center><img class="img-responsive" src="<?= base_url("assets/imagenes/banco-cordoba.png"); ?>" alt="Banco de Cordoba"></center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>
                      SUC: 919 – SAN MARTÍN           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>
                      Cta. Cte. $ Nº 0400056/03           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>          
                      CBU: 0200919701000040005633           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>      
                      Titular: PLAZA MOTOS S.A.                                
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>         
                      CUIT: 30-71023848-7
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="row">
          <div class="table-responsive col-xs-12 col-sm-12 col-md-3">
            <table class="table table-bordered">
              <thead>
                <colgroup span="1"></colgroup>
                <tr class="tableHead">
                  <th colspan="2" scope="colgroup" class="cabecera-tabla"><center><b>Banco Macro</b></center></th>
                </tr> 
              </thead>

              <tbody>
                <tr>
                  <td style="background-color: #F7F7F8 !important;">
                    <center><img class="img-responsive" src="<?= base_url("assets/imagenes/banco-macro.png"); ?>" alt="Banco Macro"></center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>
                      SUC: 381 – PATIO OLMOS            
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>
                      Cta. Cte. $ Nº 3-381-0940561437-7                                
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>          
                      CBU: 2850381130094056143771           
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font" style="background-color: #F7F7F8 !important;">
                    <center>      
                      Titular: PLAZA MOTOS S.A.                                
                    </center>
                  </td>
                </tr>
                <tr>
                  <td class="white-font">
                    <center>         
                      CUIT: 30-71023848-7           
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>