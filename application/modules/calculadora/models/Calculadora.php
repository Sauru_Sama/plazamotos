<?php
Class Calculadora extends CI_Model
{
  function consultarOperaciones($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, operacion as text FROM operaciones WHERE operacion LIKE '%".$query."%' ORDER BY id ASC;";       
    } else {
      $sql = "SELECT id, operacion as text FROM operaciones ORDER BY id ASC;";       
    }    

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarMarcas($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, marca as text FROM marcas WHERE marca LIKE '%".$query."%' ORDER BY id ASC;";       
    } else {
      $sql = "SELECT id, marca as text FROM marcas ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarModelos($query,$marcaId){
    if (isset($query) && $query != null){
      $sql = "SELECT id, modelo as text FROM modelos WHERE modelo LIKE '%".$query."%' ORDER BY id ASC;";      
    } else if (isset($marcaId) && $marcaId != null){
      $sql = "SELECT id, modelo as text FROM modelos WHERE marca_id =".$marcaId." ORDER BY id ASC;";      
    } else {
      $sql = "SELECT id, modelo as text FROM modelos ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarModelosPrecios($modeloId, $operacionId){
    $sql = "SELECT ".$operacionId." as monto, CDO_PUBLICO as cdo, categoria as categoria, flete as flete, accesorio as accesorio, descripcion as descripcion FROM modelos WHERE id = ".$modeloId.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarGestiones($query, $operacionId){
    if (isset($query) && $query != null){
      $sql = "SELECT id, gestion as text FROM gestiones WHERE gestion LIKE '%".$query."%' ORDER BY id ASC;";  
    } else if (isset($operacionId) && $operacionId != null){
      $sql = "SELECT id, gestion as text FROM gestiones WHERE operacion_id =".$operacionId." ORDER BY id ASC;"; 
    } else {
      $sql = "SELECT id, gestion as text FROM gestiones ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarGestionesPrecios($gestionId){
    $sql = "SELECT monto as monto FROM gestiones WHERE id = ".$gestionId.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarPatentamientos($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, patentamiento as text FROM patentamientos WHERE patentamiento LIKE '%".$query."%' ORDER BY id ASC;";       
    } else {
      $sql = "SELECT id, patentamiento as text FROM patentamientos ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarPatentamientosPrecios($patentamientoId){
    $sql = "SELECT monto as monto, factor as factor FROM patentamientos WHERE id = ".$patentamientoId.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarAccesorios($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, accesorio as text FROM accesorios WHERE accesorio LIKE '%".$query."%' ORDER BY id ASC;";       
    } else {
      $sql = "SELECT id, accesorio as text FROM accesorios ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarAccesoriosPrecios($accesorioId){
    $sql = "SELECT monto as monto FROM accesorios WHERE id = ".$accesorioId.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarGarantias($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, garantia as text FROM garantias WHERE garantia LIKE '%".$query."%' ORDER BY id ASC;";       
    } else {
      $sql = "SELECT id, garantia as text FROM garantias ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarGarantiasPrecios($garantiaId){
    $sql = "SELECT factor as factor FROM garantias WHERE id = ".$garantiaId.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarExtras($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, extra as text FROM extras WHERE extra LIKE '%".$query."%' ORDER BY id ASC;";       
    } else {
      $sql = "SELECT id, extra as text FROM extras ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarExtrasPrecios($extraId){
    $sql = "SELECT monto as monto FROM extras WHERE id = ".$extraId.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarSolicitados($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, solicitado as text FROM solicitados WHERE solicitado LIKE '%".$query."%' ORDER BY id ASC;";
    } else {
      $sql = "SELECT id, solicitado as text FROM solicitados ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarSolicitadosPrecios($solicitadoId){
    $sql = "SELECT monto as monto FROM solicitados WHERE id = ".$solicitadoId.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarTarjetas($query){
    if (isset($query) && $query != null){
      $sql = "SELECT id, tarjeta as text FROM tarjetas WHERE tarjeta LIKE '%".$query."%' ORDER BY id ASC;";       
    } else {
      $sql = "SELECT id, tarjeta as text FROM tarjetas ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarCuotas($query, $tarjeta){
    if (isset($query) && $query != null){
      $sql = "SELECT id, plan as text FROM tarjetas_planes WHERE plan LIKE '%".$query."%' ORDER BY id ASC;";      
    } else if (isset($tarjeta) && $tarjeta != null){
      $sql = "SELECT id, plan as text FROM tarjetas_planes WHERE tarjeta_id =".$tarjeta." ORDER BY id ASC;";      
    } else {
      $sql = "SELECT id, plan as text FROM tarjetas_planes ORDER BY id ASC;";     
    } 

    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 

  function consultarCuotasCoeficientes($cuota){
    $sql = "SELECT coeficiente as coeficiente, cuota as cuota, plan as plan FROM tarjetas_planes WHERE id = ".$cuota.";";     
    
    $query = $this->db->query($sql);     

    if ($query->num_rows() != 0){
      return $query->result();
    }//if   
    else{
      return false;
    }
    $sql->free_result();
  }//function

  //---------------------------------------------------------------------- 
  
}//class
?>