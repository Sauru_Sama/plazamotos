<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_inicio extends MX_Controller {
	function __construct(){
		parent::__construct();
		/* ------- h_plantilla_helper ------- */
		$this->load->helper('h_plantilla');
		/* ------- ./h_plantilla_helper ------- */

		/* ------- form_validation ------- */
		$this->load->library('form_validation');
	    $this->form_validation->CI =& $this;
	    /* ------- ./form_validation ------- */

	    /* ------- Modelo MENUS ------- */
	    $this->load->model('menus');
	    /* ------- ./Modelo MENUS ------- */

	}//function
//----------------------------------------------------------------------
	function index(){
		/* ------- Arreglo de Menú ------- */
		$arr_b_m = h_getMenu();
		/* ------- ./Arreglo de Menú ------- */

		/* ------- Carga la Librería Menu ------- */
		$this->load->library('Menu');
		/* ------- ./Carga la Librería Menu ------- */
	
		/* ------- Envía el arreglo de Menu a la Vista ------- */
		$data['el_menu'] = $this->menu->bootstrap_menu($arr_b_m);
		/* ------- ./Envía el arreglo de Menu a la Vista ------- */

		/* ------- Carga de Imágenes usando la libreria html_helper ------- */
		$data['img_fondo_encabezado'] = h_img_encabezado();
		$data['img_fondo_plazamotos'] = h_img_plazamotos();
		$data['img_plazamotos_10'] = h_img_plazamotos_10();
		$data['img_about'] = h_img_about();
		$data['img_fondo_encabezado_2'] = h_img_encabezado_2();
		/* ------- ./Carga de Imágenes usando la libreria html_helper ------- */

		/* ------- Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
		$this->load->view('plantillas/header',$data);
		$this->load->view('inicio/inicio');
		$this->load->view('plantillas/footer');
		/* ------- ./Carga Las Vistas en orden: Cabecera, Contenido y Pie de Página ------- */
	}//function
//----------------------------------------------------------------------
	function cerrarSesion(){
	 	$this->session->unset_userdata('Conectado');
	 	session_unset();
	  	session_destroy();
	  	redirect('login/c_verifica_login', 'refresh');
	}
//----------------------------------------------------------------------
}//class