<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div class="table-responsive col-xs-12 col-sm-12 col-md-9">
	<table class="table">
		<tbody>
			<tr>
				<td style="vertical-align: middle; background-color: #F7F7F8 !important;"><center><?=img($img_about) ?></center></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="table-responsive col-xs-12 col-sm-12 col-md-3">
	<table class="table table-bordered">
		<thead>
			<colgroup span="1"></colgroup>
			<tr class="tableHead">
			    <th colspan="3" scope="colgroup" class="cabecera-tabla"><center><b>Opciones de Administraci&oacute;n</b></center></th>
			</tr>
		</thead>

  	<tbody>
  		<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('accesorio/c_accesorio'); ?>" id="btn-accesorios" type="button" class="btn btn-danger btn-block"><b>ACCESORIOS</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('extra'); ?>" id="btn-extras" type="button" class="btn btn-danger btn-block"><b>EXTRAS</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('garantia'); ?>" id="btn-garantias" type="button" class="btn btn-danger btn-block"><b>GARANT&Iacute;AS</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('gestion'); ?>" id="btn-gestiones" type="button" class="btn btn-danger btn-block"><b>GESTIONES</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('marca'); ?>" id="btn-marcas" type="button" class="btn btn-danger btn-block"><b>MARCAS</b></a></center></td>
			</tr>

    	<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('modelo'); ?>" id="btn-modelos" type="button" class="btn btn-danger btn-block"><b>MODELOS</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('operacion'); ?>" id="btn-operaciones" type="button" class="btn btn-danger btn-block"><b>OPERACIONES</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('patentamiento'); ?>" id="btn-patentamientos" type="button" class="btn btn-danger btn-block"><b>PATENTAMIENTOS</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('usuario'); ?>" id="btn-usuarios" type="button" class="btn btn-danger btn-block"><b>USUARIOS</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('plan'); ?>" id="btn-planes" type="button" class="btn btn-danger btn-block"><b>PLANES</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('solicitado'); ?>" id="btn-solicitados" type="button" class="btn btn-danger btn-block"><b>SOLICITADOS</b></a></center></td>
			</tr>

			<tr>
				<td colspan="2"><center><a type="button" href="<?php echo site_url('tarjeta'); ?>" id="btn-tarjetas" type="button" class="btn btn-danger btn-block"><b>TARJETAS</b></a></center></td>
			</tr>
			
    </tbody>
	</table>
</div>

<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////// -->