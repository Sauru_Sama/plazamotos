<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
		<title><?=h_getTitulo_Pagina() ?></title>
		<!-- ************** FAVICON ************************************************************************************ -->
		<link rel="icon" href="<?=base_url("assets/imagenes/favicon.ico")?>">
		<!-- ************** ./FAVICON ************************************************************************************ -->

		<!-- ************** CSS ************************************************************************************ -->	
			<link rel="stylesheet" href="<?= base_url("assets/css/bootstrap.css"); ?>" />
			<!-- Estilos Personalizados/Custom-->
			<link rel="stylesheet" href="<?= base_url("assets/css/custom_estilos.css"); ?>" />
			<!-- ./Estilos Personalizados/Custom-->
		<!-- ************** ./CSS ************************************************************************************ -->	

		<!-- ************** JAVASCRIPT ******************************************************************************* -->	
			<script type="text/javascript" src="<?= base_url("assets/js/jquery-2.2.0.min.js"); ?>"></script>
			<script type="text/javascript" src="<?= base_url("assets/js/bootstrap.js"); ?>"></script>
		<!-- ************** ./JAVASCRIPT ******************************************************************************* -->	

		<!-- ************** SWEET ALERT ************************************************************************************ -->
			<script type="text/javascript" src="<?= base_url("assets/sweetalert/sweetalert.min.js"); ?>"></script>
			<link media="all" rel="stylesheet" type="text/css" href="<?= base_url("assets/sweetalert/sweetalert.css"); ?>" />
			<script type="text/javascript" src="<?= base_url("assets/notificaciones_sweetalert/notificaciones.js"); ?>"></script>
		<!-- ************** ./SWEET ALERT ********************************************************************************** -->

		<!-- ************** ct-navbar ******************************************************************************* -->	
			<link href="<?= base_url("assets/css/pe-icon-7-stroke.css"); ?>" rel="stylesheet" />
			<link href="<?= base_url("assets/css/font-awesome.min.css"); ?>" rel="stylesheet" />
   	<!-- ************** ./ct-navbar ******************************************************************************* -->	

	</head>
	<body>
		<div id="container">
			<header>
			<nav class="navbar navbar-ct-red navbar-fixed-top" role="navigation">
				<div>
					<!--header section -->
				  <div class="col-xs-4 col-sm-4"><?=img($img_fondo_encabezado_2) ?></div>
				  <div class="col-xs-4 col-sm-4"><?=img($img_fondo_encabezado_2) ?></div>
				  <div class="navbar-header col-xs-4 col-sm-4"><?=img($img_fondo_encabezado) ?></div>			  
				</div><!-- ./container-fluid -->
			</nav><!-- ./navbar navbar-default -->
			</header>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contenedor_contenido squares">
				<div id="body">
					<!-- Con Helper de librerias -->
					<center><?=img($img_app_logo) ?></center>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">