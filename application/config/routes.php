<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'calculadora/c_calculadora';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'login/c_verifica_login';

$route['inicio']                      = "inicio/c_inicio";

$route['usuario']                     = "usuario/c_usuario";
$route['usuario/nuevo']               = "usuario/c_usuario/cargarUsuarioRegistro";
$route['usuario/editar/(:num)']       = "usuario/c_usuario/editarUsuario";

$route['accesorio']                   = "accesorio/c_accesorio";
$route['accesorio/nuevo']             = "accesorio/c_accesorio/cargarAccesorioRegistro";
$route['accesorio/editar/(:num)']     = "accesorio/c_accesorio/editarAccesorio";

$route['extra']                       = "extra/c_extra";
$route['extra/nuevo']                 = "extra/c_extra/cargarExtraRegistro";
$route['extra/editar/(:num)']         = "extra/c_extra/editarExtra";

$route['solicitado']                  = "solicitado/c_solicitado";
$route['solicitado/nuevo']            = "solicitado/c_solicitado/cargarSolicitadoRegistro";
$route['solicitado/editar/(:num)']    = "solicitado/c_solicitado/editarSolicitado";

$route['garantia']                    = "garantia/c_garantia";
$route['garantia/nuevo']              = "garantia/c_garantia/cargarGarantiaRegistro";
$route['garantia/editar/(:num)']      = "garantia/c_garantia/editarGarantia";

$route['patentamiento']               = "patentamiento/c_patentamiento";
$route['patentamiento/nuevo']         = "patentamiento/c_patentamiento/cargarPatentamientoRegistro";
$route['patentamiento/editar/(:num)'] = "patentamiento/c_patentamiento/editarPatentamiento";

$route['gestion']                     = "gestion/c_gestion";
$route['gestion/nuevo']               = "gestion/c_gestion/cargarGestionRegistro";
$route['gestion/editar/(:num)']       = "gestion/c_gestion/editarGestion";

$route['plan']                        = "plan/c_plan";
$route['plan/nuevo']                  = "plan/c_plan/cargarPlanRegistro";
$route['plan/editar/(:num)']          = "plan/c_plan/editarPlan";

$route['modelo']                      = "modelo/c_modelo";
$route['modelo/nuevo']                = "modelo/c_modelo/cargarModeloRegistro";
$route['modelo/editar/(:num)']        = "modelo/c_modelo/editarModelo";

$route['marca']                       = "marca/c_marca";
$route['marca/nuevo']                 = "marca/c_marca/cargarMarcaRegistro";
$route['marca/editar/(:num)']         = "marca/c_marca/editarMarca";

$route['tarjeta']                     = "tarjeta/c_tarjeta";
$route['tarjeta/nuevo']               = "tarjeta/c_tarjeta/cargarTarjetaRegistro";
$route['tarjeta/editar/(:num)']       = "tarjeta/c_tarjeta/editarTarjeta";

$route['operacion']                   = "operacion/c_operacion";
$route['operacion/nuevo']             = "operacion/c_operacion/cargarOperacionRegistro";
$route['operacion/editar/(:num)']     = "operacion/c_operacion/editarOperacion";