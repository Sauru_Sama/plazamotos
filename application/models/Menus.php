<?php
  Class Menus extends CI_Model
  {
    function consultarMenu(){
      //-------------------------- Se verifica que tenga acceso, se encuentre habilitado y que su rol lo permita
      $sql = "SELECT a.id, a.parent_id, a.title, a.icon, a.url 
                              FROM modulos a
                              WHERE a.habilitado = 'true'
                              ORDER BY a.secuencia ASC";     

      $query = $this->db->query($sql);     

      if ($query->num_rows() == 0){
        return 1;
        //return $query->result();
      }//if   
      else{

        $arreglo_menus = array();

        $arreglo_menus = $query->result_array();

        return ($arreglo_menus);
      }

      $sql->free_result();
    
    }//function

    //---------------------------------------------------------------------- 

  }//class
?>