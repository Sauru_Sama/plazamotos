// ------------------------------------------------------------------------------
// LOGIN  
function error_login(){
	sweetAlert("Acceso Denegado.", "Valide su indicador y contraseña.", "error");
}

function sesion_concurrente(){
	sweetAlert("Acceso Denegado.", "Sesión Concurrente", "error");
}
// ------------------------------------------------------------------------------
// ./LOGIN  

// ------------------------------------------------------------------------------
// SESION EXPIRADA  
function sesion_expirada(baseurl){
	sweetAlert("Han Transcurrido 20 minutos desde la ultima vez que usó la aplicación.", "Debe volver a iniciar sesión.");

	sweetAlert({
	  title: "Sesión Expirada",
	  text: "Han Transcurrido 20 minutos desde la ultima vez que usó la aplicación.",
	  type: "warning",
	  showCancelButton: false,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: "Iniciar Sesión",
	  closeOnConfirm: true
	},
	function(){
	  top.location.href = baseurl+'index.php/inicio/c_inicio/cerrarSesion';
	  parent.window.close();
	});
}
// ------------------------------------------------------------------------------
// SESION EXPIRADA  

// ------------------------------------------------------------------------------
// ERROR CÉDULA  
function cedula_existe(){
	sweetAlert("Esta cédula ya existe en el sistema.", "Intente de nuevo.", "error");
}
// ------------------------------------------------------------------------------
// ERROR CÉDULA  

// ------------------------------------------------------------------------------
// DATOS GUARDADOS - DATOS GENERALES  
function exito_guardar(boton=null){
	sweetAlert("Exito.", "Los datos han sido guardados exitosamente", "success");
	if(boton != null){
		formato_deshabilitado_boton(boton);  
	}
}

function error_guardar(){
	sweetAlert("Disculpe.", "Hubo problemas al intentar guardar los datos", "error");
}

function notificacion_error(mensaje){
	sweetAlert(mensaje, "Verifique los datos", "error");
}

function notificacion_exito(mensaje){
	sweetAlert("Exito.", mensaje, "success");
}

function notificacion_informacion(mensaje){
	sweetAlert(mensaje, "Verifique los datos", "info");
}
//---------------------------------
// Indica que el campo a registrar esta duplicado. Coloca en rojo el objeto con el valor
//---------------------------------
function registro_existente(campo,valor,campo_id=null){
	sweetAlert("Existe un registro "+campo+" con el valor "+valor+".", "Verifique los datos", "error");
	if(campo_id != null){
		$("#"+campo_id).addClass("rojo-requerido");
	}
}
//---------------------------------
// Indica si desea descartar los cambios
// Opcion: permit definir si redirecciona a una pagina o limpia el modulo.Redireccionar=1, limpiar=2
//---------------------------------
function registro_descartar(opcion,ruta,formulario=null,datos=null){
	sweetAlert({
                title: "¿Desea descartar los cambios?",
                text: "Esta acción no se puede deshacer",
                type: "info",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, estoy seguro.",
                closeOnConfirm: false
            },
            function(){
            	if(opcion == 1){
                	window.open(ruta,"_parent");                
                }
                else if(opcion == 2){
                	inicializar_formulario(formulario,datos);                	
                }
            }//function
        );
}
// ------------------------------------------------------------------------------
// ./DATOS GUARDADOS - DATOS GENERALES  

// ------------------------------------------------------------------------------
// PROCESANDO INFORMACION CON SPINNER  
function procesando_informacion(){
	sweetAlert({
		title: "Procesado Información...",
		text: "<div class='sk-folding-cube'><div class='sk-cube1 sk-cube'></div><div class='sk-cube2 sk-cube'></div><div class='sk-cube4 sk-cube'></div><div class='sk-cube3 sk-cube'></div></div>",
	    html: true,
		timer: 2000,
		showConfirmButton: false
	});	
}
// ------------------------------------------------------------------------------
// ./PROCESANDO INFORMACION CON SPINNER  

// ------------------------------------------------------------------------------
// ELIMINAR DATOS  
function exito_eliminar(){
	sweetAlert("Exito","Se eliminaron satisfacoriamente los datos.", "success");
}

function error_eliminar(){
	sweetAlert("Disculpe", "Ocurrio un problema al intentar eliminar los datos", "error");
}

function duplicado_eliminar(){
	sweetAlert("Disculpe", "Existen Datos asociados a este Registro", "error");
}
// ------------------------------------------------------------------------------
// ./ELIMINAR DATOS  

// ------------------------------------------------------------------------------
// ERROR ROL  
function rol_existe(){
	sweetAlert("Este rol ya existe en el sistema.", "Intente de nuevo.", "error");
}
// ------------------------------------------------------------------------------
// ERROR ROL  

// ------------------------------------------------------------------------------
// ERROR REGION
function region_existe(){
	sweetAlert("Esta Región ya existe en el sistema.", "Intente de nuevo.", "error");
}
// ------------------------------------------------------------------------------
// ERROR REGION  

// ------------------------------------------------------------------------------
// ERROR ALMACEN  
function almacen_existe(){
	sweetAlert("Este Almacen ya existe en el sistema.", "Intente de nuevo.", "error");
}
// ------------------------------------------------------------------------------
// ERROR ALMACEN  

// ------------------------------------------------------------------------------
// ERROR 
function error(){
	sweetAlert("Ha ocurrido un error.", "Por favor intente de nuevo.", "error");
}
// ------------------------------------------------------------------------------
// ERROR

// ------------------------------------------------------------------------------
// ERROR CANTIDAD DESPACHO MAYOR A DISPONIBLE 
function error_cantidad_despacho(){
	sweetAlert("Ha ocurrido un error.", "La cantidad a despachar no puede ser mayor a la cantidad disponible del artículo. Por favor intente de nuevo.", "error");
}
// ------------------------------------------------------------------------------
// ERROR CANTIDAD DESPACHO MAYOR A DISPONIBLE

// ------------------------------------------------------------------------------
// DESPACHAR EXITOSO
function despacho_exitoso(){
	sweetAlert({
	  title: "Exito!",
	  text: "El Artículo ha sido despachado.<br><i class='fa fa-truck fa-5x'></i>",
	  html: true,
	  closeOnConfirm: true
	},null);
}
// ------------------------------------------------------------------------------
// DESPACHAR EXITOSO

// ------------------------------------------------------------------------------
// ACTIVAR EVENTOS  
function exito_activar(){
	sweetAlert("Exito","Se ha activado el evento satisfactoriamente.", "success");
}

function error_activar(){
	sweetAlert("Disculpe", "Problemas al activar el evento.", "error");
}
// ------------------------------------------------------------------------------
// ./ACTIVAR EVENTOS  

// ------------------------------------------------------------------------------
// DESACTIVAR EVENTOS  
function exito_desactivar(){
	sweetAlert("Exito","Se ha desactivado el evento satisfactoriamente.", "success");
}

function error_desactivar(){
	sweetAlert("Disculpe", "Problemas al desactivar el evento.", "error");
}
// ------------------------------------------------------------------------------
// ./DESACTIVAR EVENTOS  

// ------------------------------------------------------------------------------
// CERRAR EVENTOS  
function exito_cerrar(){
	sweetAlert("Exito","Se ha cerrado el evento satisfactoriamente.", "success");
}

function error_cerrar(){
	sweetAlert("Disculpe", "Problemas al cerrar el evento.", "error");
}
// ------------------------------------------------------------------------------
// ./CERRAR EVENTOS  